#include "LPC17xx.h"
#include "LCD_DRIVER.h"

void setChips()//author: ian
{
	initPins();//Alle pins in GPIO zetten,de juiste richting en de mogleijkheid om ze te manipuleren
	LPC_GPIO1->FIOSET |= (0b1 <<30);//bit a0 van de bus zetten volledige code is 1001, of de code om commandos te zetten
	LPC_GPIO1->FIOCLR |= (0b1 <<31);//bit a1
	LPC_GPIO1->FIOCLR |= (0b1 <<5);//bit a2
	LPC_GPIO1->FIOSET |= (0b1 <<4);//bit a3
	sendCommand(0xC0);//ram op het begin zetten
	sendCommand(0x40);//kolom pointer op nul zetten
	sendCommand(0xB8);//de pagina pointer op nul zetten
	sendCommand(0x3F);//display aan zetten
}

void sendCommand(uint8_t command)//author: ian
{
	LPC_GPIO1->FIOCLR |= (0b1 <<30);//bit A0 zetten, volledige code is  1000, code om commando lijn laag te zetten
	LPC_GPIO1->FIOCLR |= (0b1 <<31);//bit A1
	LPC_GPIO1->FIOCLR |= (0b1 <<5);//bit A2
	LPC_GPIO1->FIOSET |= (0b1 <<4);//bit A3
	sendByte(command);//commando op de databus zetten
	LPC_GPIO1->FIOSET |= (0b1 <<30);//commando inlezen
	LPC_GPIO1->FIOCLR |= (0b1 <<30);//commando deactiveren

}

void sendData(int chip, uint8_t data)//author: ian
{
	if(chip==1)//left chip
	{
		LPC_GPIO1->FIOCLR |= (0b1 <<30);//bit A0 zetten, volledige code is 1010, code om LCD links enable laag te zetten
		LPC_GPIO1->FIOSET |= (0b1 <<31);//bit A1
		LPC_GPIO1->FIOCLR |= (0b1 <<5);//bit A2
		LPC_GPIO1->FIOSET |= (0b1 <<4);//bit A3
		sendByte(data);//data op de databus zetten
		LPC_GPIO1->FIOSET |= (0b1 <<30);//data inlezen
		LPC_GPIO1->FIOCLR |= (0b1 <<30);//data deactiveren
	}
	else //right chip
	{
		LPC_GPIO1->FIOCLR |= (0b1 <<30);//bit A0 zetten, volledige code is 1100, code om LCD rechts enable laag te zetten
		LPC_GPIO1->FIOCLR |= (0b1 <<31);//bit A1
		LPC_GPIO1->FIOSET |= (0b1 <<5);//bit A2
		LPC_GPIO1->FIOSET |= (0b1 <<4);//bit A3
		sendByte(data);//data op de databus zetten
		LPC_GPIO1->FIOSET |= (0b1 <<30);//data inlezen
		LPC_GPIO1->FIOCLR |= (0b1 <<30);//data uitlezen
	}
}

void initPins()//author: ian
{
	LPC_PINCON->PINSEL1 &= ~(0b11<<14);//0.23 strobe in GPIO modus zetten
	LPC_PINCON->PINSEL1 &= ~(0b11<<16);//0.24 data in GPIO modus zetten
	LPC_PINCON->PINSEL1 &= ~(0b11<<18);//0.25 clock in GPIO modus zetten
	LPC_PINCON->PINSEL1 &= ~(0b11<<20);//0.26 output enable in GPIO modus zetten
	LPC_PINCON->PINSEL3 &= ~(0b11<<28);//1.30 A0 in GPIO modus zetten
	LPC_PINCON->PINSEL3 &= ~(0b11<<30);//1.31 A1 in GPIO modus zetten
	LPC_PINCON->PINSEL4 &= ~(0b11<<10);//2.5 A2 in GPIO modus zetten
	LPC_PINCON->PINSEL4 &= ~(0b11<<8);//2.4 A3 in GPIO modus zetten

	LPC_GPIO0->FIODIR |= (0b1<<23);//pin in output modus zetten
	LPC_GPIO0->FIODIR |= (0b1<<24);//pin in output modus zetten
	LPC_GPIO0->FIODIR |= (0b1<<25);//pin in output modus zetten
	LPC_GPIO0->FIODIR |= (0b1<<26);//pin in output modus zetten
	LPC_GPIO1->FIODIR |= (0b1<<30);//pin in output modus zetten
	LPC_GPIO3->FIODIR |= (0b1<<31);//pin in output modus zetten
	LPC_GPIO2->FIODIR |= (0b1<<5);//pin in output modus zetten
	LPC_GPIO2->FIODIR |= (0b1<<4);//pin in output modus zetten

	LPC_GPIO0->FIOMASK &= ~(0b1<<23);//ervoor zorgen dat de registers die de pin zetten/resetten  gemanipuleerd kan worden
	LPC_GPIO0->FIOMASK &= ~(0b1<<24);//ervoor zorgen dat de registers die de pin zetten/resetten  gemanipuleerd kan worden
	LPC_GPIO0->FIOMASK &= ~(0b1<<25);//ervoor zorgen dat de registers die de pin zetten/resetten  gemanipuleerd kan worden
	LPC_GPIO0->FIOMASK &= ~(0b1<<26);//ervoor zorgen dat de registers die de pin zetten/resetten  gemanipuleerd kan worden
	LPC_GPIO1->FIOMASK &= ~(0b1<<30);//ervoor zorgen dat de registers die de pin zetten/resetten  gemanipuleerd kan worden
	LPC_GPIO1->FIOMASK &= ~(0b1<<31);//ervoor zorgen dat de registers die de pin zetten/resetten  gemanipuleerd kan worden
	LPC_GPIO2->FIOMASK &= ~(0b1<<5);//ervoor zorgen dat de registers die de pin zetten/resetten  gemanipuleerd kan worden
	LPC_GPIO2->FIOMASK &= ~(0b1<<4);//ervoor zorgen dat de registers die de pin zetten/resetten  gemanipuleerd kan worden
}

void resetLCD()//author: ian
{
	initPins();//Alle pins in GPIO zetten,de juiste richting en de mogleijkheid om ze te manipuleren
	LPC_GPIO1->FIOCLR |= (0b1 <<30);//bit A0 zetten,code 1110, reset van de lcd
	LPC_GPIO1->FIOSET |= (0b1 <<31);//bit A1
	LPC_GPIO1->FIOSET |= (0b1 <<5);//bit A2
	LPC_GPIO1->FIOSET |= (0b1 <<4);//bit A3
}

void sendByte(uint8_t teVerzByte)//author: ian
{
	LPC_GPIO0->FIOSET |= (0b1<<26);//output enablen van de serial to parrallel
	LPC_GPIO0->FIOCLR |= (0b1<<23);//strobe afzetten  zodat de output niet veranderd wordt
	for(int i =7;i<0;i--)//alle pixels overlopen
	{
		if((teVerzByte&(0b1<<i)) ==0)//als de pixel niet gekleurd moet zijn zet de datapin op nul
		{
			LPC_GPIO0->FIOCLR |= (0b1<<24);//data op nul zetten
			dataClock();//data in de ic clocken
		}
		else
		{
			LPC_GPIO0->FIOSET |= (0b1<<24);//data pin op 1 zetten
			dataClock();//data in de ic clocken
		}
	}
	LPC_GPIO0->FIOSET |= (0b1<<23); //strobe aanzetten zodat de output veranderd wordt
	dataClock();//data inclocken
	LPC_GPIO0->FIOCLR |= (0b1<<23);//strobe afzetten zodat de output  niet veranderd
}

void dataClock()//author: ian
{
	LPC_GPIO0->FIOSET |= (0b1<<25);//clock pin zetten
	LPC_GPIO0->FIOCLR |= (0b1<<25);//clock pin uitzetten , beide vormen een clock pulse
}
