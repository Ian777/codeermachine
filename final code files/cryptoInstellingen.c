#include "cryptoInstellingen.h"

char* getStartCharacters()//author: ian
{
	static char startCharacters[3];//variabele die de startkarakters bijhoudt
	return startCharacters;//terugeven van de pointer waar de array is opgeslagen
}

void setStartCharacters(char* setStartCharacters)//author: ian
{
	char* startCharactersArray=getStartCharacters(); //opvragen van de pointer naar de array die  de startcharactersarray opvraagt
	for(int i =0;i<3;i++)
	{
		startCharactersArray[i]=setStartCharacters[i];//alle karakters die ingegeven zijn opslaan
	}
}

int* getOffSetRotors()//author: ian
{
	static int offsetRotors[3]; // variabele die de offsetkarakaters bijhoudt
	return offsetRotors; //terugeven van de pointer waar de array is opgeslagen
}

void setOffsetInstelling(int* setArray)//author: ian
{
	int* offsetArray= getOffSetRotors();//opvragen van de pointer naar de array
	offsetArray[0]=setArray[0];//opslaan van de opgegeven waarde
	offsetArray[1]=setArray[1];
	offsetArray[2]=setArray[2];
}

char* getLetterpaar()//author: ian
{
	static char letterParen[20];//variabele die alle karakters van de letterparen bijhoudt
	return letterParen;//teruggeven van de pointer waar de array is opgeslagen
}

void setLetterpaar(char* letterParenInput)//author: ian
{
	char* letterArray = getLetterpaar();//opvragen van de pointer naar de array
	for(int i = 0;i<20;i++)
	{
		letterArray[i]=letterParenInput[i];//opslaan van de letter
	}
}

int* getMaxChar()//author: ian
{
	static int maxChar;//variable die het maximum aantal karakters bijhoudt
	return &maxChar;//terugeven van de pointer naar de variable met het maximum aantal karakters
}

void setMaxChar(int maxChar)//author: ian
{
	int* referenceToMaxChar = getMaxChar();//opvragen van de pointer naar de int waar het maximum aantal karakters is opgeslagen
	*referenceToMaxChar=maxChar;//opslaan van het nieuwe aantal maximum karakters
}


