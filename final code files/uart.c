#include "UART.H"


void setUpUART()//author: ian
{
	LPC_UART0->LCR |= (1<<7);//mogelijk maken van het aanpassen van de controle registers
	LPC_UART0->DLL = 104;//UnDLLparameter van de baudrate
	LPC_UART0->DLM = 0;//UnDLM parameter
	LPC_UART0->FDR = 1;//divaddval
	LPC_UART0->FDR = (0b1<<4);//mulval
	LPC_UART0->LCR &=~(1<<7);//onmogleijk maken van het aanpassen van de controle registers
	LPC_UART0->FCR |= 1;//activeren van receiver en transmitter FIFO's
	LPC_PINCON->PINSEL0 |= (5 << 4);//Activeren van de receiver en de transmitter
	LPC_PINCON->PINMODE0 |= (1<<7);//pulldown weerstand op de receiver uit zetten
}

char getUARTCharacter()//author: ian
{
	while((LPC_UART0->LSR & 1)==0);//voorkomt dat je data overschrijft tijdens het lezen of schrijven
	char received=LPC_UART0->RBR;//ontvangen karakter opslaan in een tijdelijke variabele
	return received;//ontvangen karakter teruggeven
}

void putUARTCharacter(char input)//author: ian
{
	LPC_UART0->THR = input;//karakter dat je wilt zenden in de juiste register zetten
	while((LPC_UART0->LSR & (1<<6) )==0);//voorkomt dat je data overschrijft tijdens het lezen of schrijven
}
