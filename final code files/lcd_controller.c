#include"lcd_controller.h"
#include "lcd_driver.h"

void printBericht(char* bericht, int length)//author: ian
{
	resetLCD();//LCD scherm leegmaken
	counter(1);//reset counter
	setChips();//initialiseer de chips
	for(int i=0;i<length;i++ )//iterereren over alle karakters van het bericht
	{
		if(counter(0)==64)//checken of we nog steeds links mogen printen
		{
			determinePrint(bericht[i], 1);//bepalen welke print methode aangeroepen moet worden voor de linkerkant
		}
		else
		{
			determinePrint(bericht[i], 0);//bepalen welke print methode aangeroepen moet worden voor de rechterkant
		}
	}
}

void determinePrint(char x, int chip)//1 chip is links,0 rechts author: ian
{
	switch(x)
		{
			case '_'://gewenste karakter
				printBlank(chip); //aanroep van de print methode van het karakter
				break;
			case '#':
				printSelector(chip);
				break;
			case 'A':
				printA(chip);
				break;
			case 'B':
				printB(chip);
				break;
			case 'C':
				printC(chip);
				break;
			case 'D':
				printD(chip);
				break;
			case 'E':
				printE(chip);
				break;
			case 'F':
				printF(chip);
				break;
			case 'G':
				printG(chip);
				break;
			case 'H':
				printH(chip);
				break;
			case 'I':
				printI(chip);
				break;
			case 'J':
				printJ(chip);
				break;
			case 'K':
				printK(chip);
				break;
			case 'L':
				printL(chip);
				break;
			case 'M':
				printM(chip);
				break;
			case 'N':
				printN(chip);
				break;
			case 'O':
				printO(chip);
				break;
			case 'P':
				printP(chip);
				break;
			case 'Q':
				printQ(chip);
				break;
			case 'R':
				printR(chip);
				break;
			case 'S':
				printS(chip);
				break;
			case 'T':
				printT(chip);
				break;
			case 'U':
				printU(chip);
				break;
			case 'W':
				printW(chip);
				break;
			case 'X':
				printX(chip);
				break;
			case 'Y':
				printY(chip);
				break;
			case 'Z':
				printZ(chip);
				break;
			default:
				printBlank(chip);
		}
}

int counter(uint8_t reset)//author: ian
{
	static int counter= 0;//variable dat bijhoudt bij welke kolom je bent
	counter++;//counter optellen
	if(counter==128||reset)//als de counter 128 wordt(maximum aantal kolommen of moet gereset worden zet de counter op nul
	{
		counter=0;
	}
	return counter;//counter teruggeven
}


void printBlank(int chip)//author: ian
{
	sendData(chip, 0x03);//doorsturen welke kant het moet geprint worden en welke pixels gekleurd moeten worden dit is hetzelfde voor alle print methodes
	sendData(chip, 0x03);
	sendData(chip, 0x03);
	sendData(chip, 0x03);
}

void printSelector//author: ian
{
	sendData(chip,0x6C);
	sendData(chip,0xFF);
	sendData(chip,0x6C);
	sendData(chip,0xFF);
}

void printA(int chip)//author: ian
{
	sendData(chip,0x00);
	sendData(chip,0x3F);
	sendData(chip,0x48);
	sendData(chip,0x3F);
}

void printB(int chip)//author: ian
{
	sendData(chip,0x00);
	sendData(chip,0x7F);
	sendData(chip,0x49);
	sendData(chip,0x36);
}

void printC(int chip)//author: ian
{
	sendData(chip,0x00);
	sendData(chip,0x1C);
	sendData(chip,0x22);
	sendData(chip,0x41);
}

void printD(int chip)//author: ian
{
	sendData(chip,0x00);
	sendData(chip,0x7F);
	sendData(chip,0x41);
	sendData(chip,0x3E);
}

void printE(int chip)//author: ian
{
	sendData(chip,0x00);
	sendData(chip,0x7F);
	sendData(chip,0x4D);
	sendData(chip,0x4D);
}

void printF(int chip)//author: ian
{
	sendData(chip,0x00);
	sendData(chip,0x7F);
	sendData(chip,0x58);
	sendData(chip,0x58);
}

void printG(int chip)//author: ian
{
	sendData(chip,0x00);
	sendData(chip,0x3E);
	sendData(chip,0x61);
	sendData(chip,0x46);
}

void printH(int chip)//author: ian
{
	sendData(chip,0x00);
	sendData(chip,0x7F);
	sendData(chip,0x0C);
	sendData(chip,0x7F);
}

void printI(int chip)//author: ian
{
	sendData(chip,0x00);
	sendData(chip,0x63);
	sendData(chip,0x7F);
	sendData(chip,0x63);
}

void printJ(int chip)//author: ian
{
	sendData(chip,0x00);
	sendData(chip,0x46);
	sendData(chip,0x41);
	sendData(chip,0x7E);
}

void printK(int chip)//author: ian
{
	sendData(chip,0x00);
	sendData(chip,0x7F);
	sendData(chip,0x12);
	sendData(chip,0x21);
}

void printL(int chip)//author: ian
{
	sendData(chip,0x00);
	sendData(chip,0x7F);
	sendData(chip,0x03);
	sendData(chip,0x03);
}

void printM(int chip)//author: ian
{
	sendData(chip,0x00);
	sendData(chip,0x7F);
	sendData(chip,0x20);
	sendData(chip,0xFE);
}

void printN(int chip)//author: ian
{
	sendData(chip,0x00);
	sendData(chip,0x7F);
	sendData(chip,0x40);
	sendData(chip,0x7F);
}

void printO(int chip)//author: ian
{
	sendData(chip,0x00);
	sendData(chip,0x3E);
	sendData(chip,0x41);
	sendData(chip,0x3E);
}

void printP(int chip)//author: ian
{
	sendData(chip,0x00);
	sendData(chip,0x7F);
	sendData(chip,0x48);
	sendData(chip,0x30);
}

void printQ(int chip)//author: ian
{
	sendData(chip,0x00);
	sendData(chip,0x1C);
	sendData(chip,0x22);
	sendData(chip,0x1D);
}

void printR(int chip)//author: ian
{
	sendData(chip,0x00);
	sendData(chip,0x7F);
	sendData(chip,0x42);
	sendData(chip,0x3D);
}

void printS(int chip)//author: ian
{
	sendData(chip,0x00);
	sendData(chip,0x09);
	sendData(chip,0x15);
	sendData(chip,0x12);
}

void printT(int chip)//author: ian
{
	sendData(chip,0x00);
	sendData(chip,0x60);
	sendData(chip,0x7F);
	sendData(chip,0x60);
}

void printU(int chip)//author: ian
{
	sendData(chip,0x00);
	sendData(chip,0x7E);
	sendData(chip,0x01);
	sendData(chip,0x7F);
}

void printV(int chip)//author: ian
{
	sendData(chip,0x00);
	sendData(chip,0x7C);
	sendData(chip,0x02);
	sendData(chip,0x7C);
}

void printW(int chip)//author: ian
{
	sendData(chip,0x00);
	sendData(chip,0x7F);
	sendData(chip,0x02);
	sendData(chip,0x7F);
}

void printX(int chip)//author: ian
{
	sendData(chip,0x00);
	sendData(chip,0x77);
	sendData(chip,0x08);
	sendData(chip,0x77);
}

void printY(int chip)//author: ian
{
	sendData(chip,0x00);
	sendData(chip,0x71);
	sendData(chip,0x09);
	sendData(chip,0x7E);
}

void printZ(int chip)//author: ian
{
	sendData(chip,0x00);
	sendData(chip,0x67);
	sendData(chip,0x6B);
	sendData(chip,0x73);
}

