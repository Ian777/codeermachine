#ifndef LCD_CONTROLLER_H_
#define LCD_CONTROLLER_H_

#include "LPC17xx.h"

/*
 * author ian
 * methode dat een bericht doorgeeft naar de controller
 * @params char* bericht: de array waarin het bericht opgeslagen word
 * @params int length: de lengte van het bericht
 */
void printBericht(char* bericht, int length);

/*
 * author ian
 * methode dat bepaald welke print methode moet worden aangeropen voor een specifieke karakter
 * @params char x: het karakter waarvan de methode moet worden bepaald
 * @params int chip: een integer die bepaald of er links of rechts geprint wordt op de lcd
 */
void determinePrint(char x, int chip);

/*
 * author ian
 * methode dat bijhoud welke chip moet geselecteerd worden, kan wordne gereset
 * @params uint8_t reset: integer dat als boolean gebruikt word
 * @return de waarde van de teller
 */
int counter(uint8_t reset);

/*
 * author ian
 * methode dat '_' print op het lcd scherm
 * @params int chip: een integer die bepaald of er links of rechts geprint wordt op de lcd
 */
void printBlank(int chip);//char _

/*
 * author ian
 * methode dat '#' print op het lcd scherm
 * @params int chip: een integer die bepaald of er links of rechts geprint wordt op de lcd
 */
void printSelector(int chip);//char #

/*
 * author ian
 * methode dat 'A' print op het lcd scherm
 * @params int chip: een integer die bepaald of er links of rechts geprint wordt op de lcd
 */
void printA(int chip);

/*
 * author ian
 * methode dat 'B' print op het lcd scherm
 * @params int chip: een integer die bepaald of er links of rechts geprint wordt op de lcd
 */
void printB(int chip);

/*
 * author ian
 * methode dat 'C' print op het lcd scherm
 * @params int chip: een integer die bepaald of er links of rechts geprint wordt op de lcd
 */
void printC(int chip);

/*
 * author ian
 * methode dat 'D' print op het lcd scherm
 * @params int chip: een integer die bepaald of er links of rechts geprint wordt op de lcd
 */
void printD(int chip);

/*
 * author ian
 * methode dat 'E' print op het lcd scherm
 * @params int chip: een integer die bepaald of er links of rechts geprint wordt op de lcd
 */
void printE(int chip);

/*
 * author ian
 * methode dat 'F' print op het lcd scherm
 * @params int chip: een integer die bepaald of er links of rechts geprint wordt op de lcd
 */
void printF(int chip);

/*
 * author ian
 * methode dat 'G' print op het lcd scherm
 * @params int chip: een integer die bepaald of er links of rechts geprint wordt op de lcd
 */
void printG(int chip);

/*
 * author ian
 * methode dat 'H' print op het lcd scherm
 * @params int chip: een integer die bepaald of er links of rechts geprint wordt op de lcd
 */
void printH(int chip);

/*
 * author ian
 * methode dat 'I' print op het lcd scherm
 * @params int chip: een integer die bepaald of er links of rechts geprint wordt op de lcd
 */
void printI(int chip);

/*
 * author ian
 * methode dat 'J' print op het lcd scherm
 * @params int chip: een integer die bepaald of er links of rechts geprint wordt op de lcd
 */
void printJ(int chip);

/*
 * author ian
 * methode dat 'K' print op het lcd scherm
 * @params int chip: een integer die bepaald of er links of rechts geprint wordt op de lcd
 */
void printK(int chip);

/*
 * author ian
 * methode dat 'L' print op het lcd scherm
 * @params int chip: een integer die bepaald of er links of rechts geprint wordt op de lcd
 */
void printL(int chip);

/*
 * author ian
 * methode dat 'M' print op het lcd scherm
 * @params int chip: een integer die bepaald of er links of rechts geprint wordt op de lcd
 */
void printM(int chip);

/*
 * author ian
 * methode dat 'N' print op het lcd scherm
 * @params int chip: een integer die bepaald of er links of rechts geprint wordt op de lcd
 */
void printN(int chip);

/*
 * author ian
 * methode dat 'O' print op het lcd scherm
 * @params int chip: een integer die bepaald of er links of rechts geprint wordt op de lcd
 */
void printO(int chip);

/*
 * author ian
 * methode dat 'P' print op het lcd scherm
 * @params int chip: een integer die bepaald of er links of rechts geprint wordt op de lcd
 */
void printP(int chip);

/*
 * author ian
 * methode dat 'Q' print op het lcd scherm
 * @params int chip: een integer die bepaald of er links of rechts geprint wordt op de lcd
 */
void printQ(int chip);

/*
 * author ian
 * methode dat 'R' print op het lcd scherm
 * @params int chip: een integer die bepaald of er links of rechts geprint wordt op de lcd
 */
void printR(int chip);

/*
 * author ian
 * methode dat 'S' print op het lcd scherm
 * @params int chip: een integer die bepaald of er links of rechts geprint wordt op de lcd
 */
void printS(int chip);

/*
 * author ian
 * methode dat 'T' print op het lcd scherm
 * @params int chip: een integer die bepaald of er links of rechts geprint wordt op de lcd
 */
void printT(int chip);

/*
 * author ian
 * methode dat 'U' print op het lcd scherm
 * @params int chip: een integer die bepaald of er links of rechts geprint wordt op de lcd
 */
void printU(int chip);

/*
 * author ian
 * methode dat 'W' print op het lcd scherm
 * @params int chip: een integer die bepaald of er links of rechts geprint wordt op de lcd
 */
void printW(int chip);

/*
 * author ian
 * methode dat 'X' print op het lcd scherm
 * @params int chip: een integer die bepaald of er links of rechts geprint wordt op de lcd
 */
void printX(int chip);

/*
 * author ian
 * methode dat 'Y' print op het lcd scherm
 * @params int chip: een integer die bepaald of er links of rechts geprint wordt op de lcd
 */
void printY(int chip);

/*
 * author ian
 * methode dat 'Z' print op het lcd scherm
 * @params int chip: een integer die bepaald of er links of rechts geprint wordt op de lcd
 */
void printZ(int chip);

#endif /* LCD_CONTROLLER_H_ */
