/*
 *H File die de statemachine methodes bevat.
 */
#ifndef STATEMACHINE_H
#define STATEMACHINE_H

#include "enigma2.h"
#include "LPC17xx.h"
#include "Input_Event.h"
#include "cryptoInstellingen.h"
#include "lcd_driver.h"


/*
 * blocking methode die de karakter teruggeeft van de pol methode in input_event
 * @returns: input karakter
 */
char validateEvent();

/*
 * methode dat de karakter dat ingeduwd is omvormd naar een nummer met A=0 en Z = 25.
 * @returns: een int tussen 0 en 25
 */
int charToNumber();

/**
 * methode die in de main wordt aangeropen om de statemachine te activeren
 */
void runStatemachine();

/*
 * de methode die moet uitgevoerd worden voor het instellen van de machine, 'instellingen' state
 */
void runCryptoInstellingen();

/*
 * de methode die gebruikt word voor te coderen en te decoderen, 'encrypteer-' en 'decrypteer' state
 */
void runCoderen();

/*
 * de methode die eerste een bericht laat typen en dan gaat coderen.
 */
void runCodeerBericht();

/*
 * methode die gebruikt wordt om de machine in te stellen bij het opstarten,'init' state
 */
void runInit();

#endif

