#include "statemachine.h"
#include "cryptoInstellingen.h"
#include "enigma2.h"
#include <stdio.h>

int charToNumber()//author: ian
{
	int check = 1;//variabele om de methode blocking te maken
	while(check)
	{
		switch(validateEvent())//methode oproep naar de blocking button event handler
		{
			case 'A'://letter dat je terugkrijgt van de buttons
				printf("keuze 0\n\r");//print met welke keuze je gemaakt hebt
				return 0;//het overeenkomstig getal terugeven
				break;
			case 'B':
				printf("keuze 1\n\r");
				return 1;
				break;
			case 'C':
				printf("keuze 2\n\r");
				return 2;
				break;
			case 'D':
				printf("keuze 3\n\r");
				return 3;
				break;
			case 'E':
				printf("keuze 4\n\r");
				return 4;
				break;
			case 'F':
				printf("keuze 5\n\r");
				return 5;
				break;
			case 'G':
				printf("keuze 6\n\r");
				return 6;
				break;
			case 'H':
				printf("keuze 7\n\r");
				return 7;
				break;
			case 'I':
				printf("keuze 8\n\r");
				return 8;
				break;
			case 'J':
				printf("keuze 9\n\r");
				return 9;
				break;
			case 'K':
				printf("keuze 10\n\r");
				return 10;
				break;
			case 'L':
				printf("keuze 11\n\r");
				return 11;
				break;
			case 'M':
				printf("keuze 12\n\r");
				return 12;
				break;
			case 'N':
				printf("keuze 13\n\r");
				return 13;
				break;
			case 'O':
				printf("keuze 14\n\r");
				return 14;
				break;
			case 'P':
				printf("keuze 15\n\r");
				return 15;
				break;
			case 'Q':
				printf("keuze 16\n\r");
				return 16;
				break;
			case 'R':
				printf("keuze 17\n\r");
				return 17;
				break;
			case 'S':
				printf("keuze 18\n\r");
				return 18;
				break;
			case 'T':
				printf("keuze 19\n\r");
				return 19;
				break;
			case 'U':
				printf("keuze 20\n\r");
				return 20;
				break;
			case 'V':
				printf("keuze 21\n\r");
				return 21;
				break;
			case 'W':
				printf("keuze 22\n\r");
				return 22;
				break;
			case 'X':
				printf("keuze 23\n\r");
				return 23;
				break;
			case 'Y':
				printf("keuze 24\n\r");
				return 24;
				break;
			case 'Z':
				printf("keuze 25\n\r");
				return 25;
				break;
			default:
				return 0;
		}
	}
	return 0;
}
char validateEvent()//author: ian
{
	int check = 1;//de methode blocking maken met behulp van deze variabele
	while(check)
	{
		char* eventChars = getEvent();//steek de eventchar pointer in de var eventChars
		switch(eventChars[0])//het eerste karakter opvragen
				{
					case 'A'://het karakter waarmee  je vergelijkt
						printf("keuze A\n\r");//tonen welk karakter er gekozen is
						return 'A';//het gekozen karater teruggeven
						break;
					case 'B':
						printf("keuze B\n\r");
						return 'B';
						break;
					case 'C':
						printf("keuze C\n\r");
						return 'C';
						break;
					case 'D':
						printf("keuze D\n\r");
						return 'D';
						break;
					case 'E':
						printf("keuze E\n\r");
						return 'E';
						break;
					case 'F':
						printf("keuze F\n\r");
						return 'F';
						break;
					case 'G':
						printf("keuze G\n\r");
						return 'G';
						break;
					case 'H':
						printf("keuze H\n\r");
						return 'H';
						break;
					case 'I':
						printf("keuze I\n\r");
						return 'I';
						break;
					case 'J':
						printf("keuze J\n\r");
						return 'J';
						break;
					case 'K':
						printf("keuze K\n\r");
						return 'K';
						break;
					case 'L':
						printf("keuze L\n\r");
						return 'L';
						break;
					case 'M':
						printf("keuze M\n\r");
						return 'M';
						break;
					case 'N':
						printf("keuze N\n\r");
						return 'N';
						break;
					case 'O':
						printf("keuze O\n\r");
						return 'O';
						break;
					case 'P':
						printf("keuze P\n\r");
						return 'P';
						break;
					case 'Q':
						printf("keuze Q\n\r");
						return 'Q';
						break;
					case 'R':
						printf("keuze R\n\r");
						return 'R';
						break;
					case 'S':
						printf("keuze S\n\r");
						return 'S';
						break;
					case 'T':
						printf("keuze T\n\r");
						return 'T';
						break;
					case 'U':
						printf("keuze U\n\r");
						return 'U';
						break;
					case 'V':
						printf("keuze V\n\r");
						return 'V';
						break;
					case 'W':
						printf("keuze W\n\r");
						return 'W';
						break;
					case 'X':
						printf("keuze X\n\r");
						return 'X';
						break;
					case 'Y':
						printf("keuze Y\n\r");
						return 'Y';
						break;
					case 'Z':
						printf("keuze Z\n\r");
						return 'Z';
						break;
					case '0':
						printf("keuze 0\n\r");
						return '0';
						break;
					default:
						break;
				}

	}
}

void runCryptoInstellingen()//author: ian
{
	int check = 1;//de state geactiveerd houden totdat de state verlaten is
	while(check)
	{
		printf("instellingen voor de codeermachine\n\r");//het menu weergeven
		printf("offset instellen(A)\n\r");
		printf("letterparen instellen(B)\n\r");
		printf("maximum aantal karakters(C)\n\r");
		printf("start karakters instellen(D)\n\r");
		printf("verlaat crypto instellingen(E)\n\r");
		int offsetSettings[3];//de variabelen instellen voor de instellingen in op te slaan terwijl ze ingegebven worden
		char letterParen[20];
		char startKarakters[3];
		int aantalLetterparen;
		char keuze = validateEvent();//menukeuze in een variabele steken
		if(keuze=='A')
		{
				printf("stel de offset in voor de drie rotors(A=0,Z=25)\n\r");//printen wat het menu doet
				printf("stel de eerste offset in(A=0,Z=25)\n\r");//instructie printen
				offsetSettings[0]=charToNumber();//input opslaan in een tijdelijke variabele
				printf("stel de tweede offset in(A=0,Z=25)\n\r");
				offsetSettings[1]=charToNumber();
				printf("stel de derde offset in(A=0,Z=25)\n\r");
				offsetSettings[2]=charToNumber();
				setOffsetInstelling(offsetSettings);//de instellingen opslaan
		}
		else if(keuze=='B')
		{
				int maximumBeveiliging=1;//variabele voor te controleren op de limiet van aantal letterpaten
				while(maximumBeveiliging==1)
				{
					printf("hoeveel Letterparen wilt u, maximum is 10(A=0,K=10)\n\r");
					aantalLetterparen = charToNumber();//input opvragen
					if((aantalLetterparen*2)<=20)//controleren of er voldaan is aan de limiet
					{
						maximumBeveiliging=0;//de methode laten voortgaan
					}
					else
					{
						printf("geef een waarde in tussen 0 en 1O\n\r");//error bericht
					}
				}
				for(int i =0;i<20;i++)
				{
					if(i<(aantalLetterparen*2))//controleren van het aantal letterparen je nog mag ingeven
					{
						if(i%2==0)//eerste karakter van het letterpaar
						{
							printf("geef het karakter (%i) dat je wil vervangen\n\r",i);//instructie tonen
							letterParen[i]=validateEvent();//input opslaan
						}
						else if(i%2!=0)//tweede karakter van het letterpaar
						{
							printf("geef het karakter (%i) waarmee het vervangen moet worden\n\r",i);//instructie tonen
							letterParen[i]=validateEvent();//input opslaan
						}
					}
					else
					{
						letterParen[i]=0;//alle niet ingevulde letterparen krijgen ene default waarde
					}
				}
				printf("%s\n\rletterParen",letterParen);//alle leterparen tonen
				setLetterpaar(letterParen);//de leterparen opslaan
		}
		else if(keuze=='C')
		{
				printf("geef het aantal cijfers in\n\r");//instructie tonen
				int limiet = charToNumber();//bepalen uit hoeveel cijfers de maxchars bestaan
				int waarde=0;
				for(int i =0;i<limiet;i++)//limiet keer een cijfer vragen
                {
                    printf("geef de waarde van cijfer %i :(A=0,Z=10)",i);//instructie tonen
                    if(i!=0)
                    {
                        waarde+=charToNumber()*10^i;//het getal maal de rang optellen aan de tijdelijke maxchar
                    }
                    else
                    {
                        waarde=charToNumber();//rang 0 aan de tijdelijke variabele toekennen
                    }
                }
				setMaxChar(waarde);//waarde opslaan
		}
		else if(keuze=='D')
		{
				printf("stell de start karakters in\n\r");//instructie tonen
				printf("stel het eerste startkarakter in\n\r");
				startKarakters[0]=validateEvent();//startkarakter opslaan
				printf("stel het tweede startkarakter in\n\r");
				startKarakters[1]=validateEvent();
				printf("stel het derde startkarakter in\n\r");
				startKarakters[2]=validateEvent();
				setStartCharacters(startKarakters);//startkarakters opslaan
		}
		else if(keuze=='E')
		{
				check=0;//ervoor zorgen dat de while lus stopt, zodat je terug naar de menu state gaat
		}
	}
}

void runCoderen()//author: ian
{
	int* offsetrotors=getOffSetRotors();//offset opvragen
	char* letterParen = getLetterpaar();//letterparen opvragen
	int maxChar = (int)*(getMaxChar());//maximum characters opvragen
	char*startcharacters = getStartCharacters();//startkarakters opvragen
	setOffset(offsetrotors[0], offsetrotors[1], offsetrotors[2]);//offset instellen
	setStartPos(startcharacters[0], startcharacters[1], startcharacters[2]);//startposities instellen
	setLetterPairs(letterParen);//letterparen instellen
	setMaxLength(maxChar);//maximum aantal karakters instellen
	intitialize();//enigma starten
	char bericht[(maxChar)];//variabele waarin het gencodeerd bericht opgeslagen wordt
	char input;//variabele waarin de input karakters wordt opgeslagen
	for(int i =0;i<maxChar;i++)
	{
	    printf("geef een character\n\r");
		input=codeer(validateEvent());//de input encrypteren
	    printf("output: %c\n\r",input);
		bericht[i]=input;//input opslaan
	}
	printf(bericht);//geëncodeerd bericht opslaan
}

void runCodeerBericht()//author: ian
{
	int* offsetrotors=getOffSetRotors();//offset opvragen
	char* letterParen = getLetterpaar();//letterparen opvragen
	int maxChar = (int)*(getMaxChar());//maximum characters opvragen
	char*startcharacters = getStartCharacters();//startkarakters opvragen
	setOffset(offsetrotors[0], offsetrotors[1], offsetrotors[2]);//offset instellen
	setStartPos(startcharacters[0], startcharacters[1], startcharacters[2]);//startposities instellen
	setLetterPairs(letterParen);//letterparen instellen
	setMaxLength(maxChar);//maximum aantal karakters instellen
	intitialize();//enigma starten
	char inputbericht[(maxChar+1)];//variabele om het input bericht op te slaan
	char outputbericht[(maxChar+1)];//variabele om het output bericht op te slaan
	char input;//variabele waarin de huidige input wordt opgeslagen
	for(int i =0;i<maxChar;i++)//voor maxchar aan karakters vragen
	{
	    printf("geef een character\n\r");
		input=(validateEvent());//input opvragen
	    printf("output: %c\n\r",input);
		inputbericht[i]=input;//input opslaan
	}
	printf("%s\n\r",inputbericht);//input printen
	codeerMessage(outputbericht, inputbericht);//input bericht encoderen
	printf("%s\n\r",outputbericht);//geëncodeerd bericht printen
}

void runInit()//author: ian
{
	int offsetArray[3];//variabele waarin de offset wordt ingesteld om dan te kunnen opslaan
	offsetArray[0]=0;//default waarde van de offset invullen
	offsetArray[1]=0;
	offsetArray[2]=0;
	setOffsetInstelling(offsetArray);//offset opslaan
	char letterPaarArray[20];//variabele waarin de letterparen worden ingesteld om dan te kunnen opslaan
	for(int i = 0;i<20;i++)
	{
		letterPaarArray[i]=0;//default waarde voor een karakter van ene letterpaar opslaan
	}
	setLetterpaar(letterPaarArray);//letterparen opslaan
	setMaxChar(5);//maximum aantal karakters invullen , 5 is voor de demo
	char startCharacters[3];//variabele waarin de start karaters worden ingesteld om dan te kunnen opslaan
	startCharacters[0]='A';//default waarde van de start karakters invullen
	startCharacters[1]='A';
	startCharacters[2]='A';
	setStartCharacters(startCharacters);//start karakters opslaan
}

void runStatemachine()//author: ian
{
	int state=-1;//idle state
	runInit();//initstate, enigma op default instellen
	while(1)
	{
		switch(state)
		{
			case -1://idle state
				printf("druk op A om de encryptie instellingen te wijzingen\n\r");//tonen welk menu er gekozen kan worden
				printf("druk op B om een bericht karakter per karakter te encoderen\n\r");
				printf("druk op C om een bericht karakter per karakter te decoderen\n\r");
				printf("druk op D om een bericht in te typen en dan te coderen\n\r");
				printf("druk op E om een bericht in te typen en dan te decoderen\n\r");
				state=charToNumber();//input in de state opslaan
				break;
			case 0:
				printf("cryptoinstellingen:\n\r");//printen welke keuze er gemaakt is
				runCryptoInstellingen();//de state activeren, state: instellingen
				state = -1;//state terug op idle zetten
				break;
			case 1:
				printf("encodeer:\n\r");
				runCoderen();//state:coderen
				state=-1;
				break;
			case 2:
				printf("decodeer:\n\r");
				runCoderen();//state:coderen
				state=-1;
				break;
			case 3:
				printf("codeer bericht:\n\r");
				runCodeerBericht();//state bericht
				state=-1;
				break;
			case 4:
				printf("decodeer bericht:\n\r");
				runCodeerBericht();//state:bericht
				state=-1;
				break;
		}
	}
}
