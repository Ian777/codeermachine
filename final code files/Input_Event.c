/*
 * Input_Event.c
 *
 *  Created on: 30 okt. 2018
 *      Author: jelle
 */

#include "Input_Event.h"

/*
 * Determines the char corresponding to the button and pin
 *
 * returns: returns the char determined by which button and which pin
 */
char getEvent(){
	int inputsarray[2] = {0, 0};
	getIdentity(inputsarray);
	printf("button %d  dip %d",inputsarray[0],inputsarray[1]);
	if(inputsarray[1]==0){
		return decideChar(inputsarray[0]);
	}
	else{
		return decideMenubutton(inputsarray[0]);
	}

}

/*
 * decides the alphabetic character depending on the button if no jumper is connected
 *
 * @param: button
 * 		button: The button determined by input_identifier
 *
 * returns: returns the character
 */
char decideChar(int button){
	switch(button){
	case 0:  return 'O'; break;
	case 1:  return 'R'; break;
	case 2:  return 'H'; break;
	case 3:  return 'D'; break;
	case 4:  return 'L'; break;
	case 5:  return 'I'; break;
	case 6:  return 'E'; break;
	case 7:  return 'A'; break;
	case 8:  return 'Z'; break;
	case 9:  return 'W'; break;
	case 10: return 'B'; break;
	case 11: return 'F'; break;
	case 12: return 'X'; break;
	case 13: return 'U'; break;
	case 14: return 'J'; break;
	case 15: return 'M'; break;
	case 16: return 'P'; break;
	case 17: return 'S'; break;
	case 18: return 'V'; break;
	case 19: return 'Y'; break;
	case 20: return 'T'; break;
	case 21: return 'Q'; break;
	case 22: return 'N'; break;
	case 23: return 'K'; break;
	case 24: return 'G'; break;
	case 25: return 'C'; break;
	default: return 0; break;
	}
}

/*
 * decides the number used for the menu depending on the button if the jumper is connected
 *
 * @param: button
 * 		button: The button determined by input_identifier
 *
 * returns: returns the character
 */
char decideMenubutton(int button){
	switch(button){
	/*case 7:  return 'A'; break;
	case 10: return '1'; break;
	case 25: return '2'; break;*/
	case 3:  return '3'; break;
	/*case 6:  return '4'; break;
	case 11: return '5'; break;
	case 24: return '6'; break;
	case 2:  return '7'; break;
	case 5:  return '8'; break;
	case 14: return '9'; break;*/
	default: return 0; break;
	}
}
