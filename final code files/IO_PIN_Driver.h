/*
 * IO_PIN_Driver.c
 *
 *  Created on: 28 nov. 2018
 *      Author: Ian
 *      	Made the first version of the functions
 *      	Due to hardwareconnections version not usable
 *      co-Author: Jelle
 *      	Edited functions to work with hardware
 */


#ifndef IO_PIN_DRIVER_H_
#define IO_PIN_DRIVER_H_
#include "LPC17xx.h"

void initialize_pins();
void direction_pins();
void setPins();
void clearPins();
int readPins();
int testRead();
void mask();
#endif /* IO_PIN_DRIVER_H_ */

/*
 * pins to power: 5 7 09 11 - 13 15 17 19 - 21 23 25 27 - 29
 * pins to read : 6 8 10 12 - 14 16 18 20 - 22 24 26 28 - 30
 */
