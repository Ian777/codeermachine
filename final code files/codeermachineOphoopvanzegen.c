/*
===============================================================================
 Name        : codeermachineOphoopvanzegen.c
 Author      : $(Ian)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>
#include "lcd_driver.h"
#include "lcd_controller.h"
#include "enigma2.h"
#include "LPC17xx.h"
#include "Input_Event.h"
#include "cryptoInstellingen.h"
#include "lcd_driver.h"
#include "statemachine.h"
#include "buttonDriver.h"
#include "UART.h"

int main(void) {
    // Enter an infinite loop, running the statemachine
	while(1)//author: ian
	{
		runStatemachine();//aanroep van de methode dat de statemachine activeert
    }
    return 0 ;
}
