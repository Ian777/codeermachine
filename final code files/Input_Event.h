/*
 * Input_Event.h
 *
 *  Created on: 30 okt. 2018
 *      Author: jelle
 */

#ifndef INPUT_EVENT_H_
#define INPUT_EVENT_H_

#include "Input_Identifier.h"

char getEvent();
char decideChar(int button);
char decideMenubutton(int button);
#endif /* INPUT_EVENT_H_ */
