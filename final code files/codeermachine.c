/*
===============================================================================
 Name        : codeermachine.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

//#include <cr_section_macros.h>
#include "buttonDriver.h"
#include "IO_PIN_Driver.h"
#include "Input_Event.h"
#include "enigma2.h"
//#include "stdio.h"

// TODO: insert other include files here

// TODO: insert other definitions and declarations here

int main(void) {

    // Enter an infinite loop, just incrementing a counter
	setOffset(0,5,0);
	setStartPos('C', 'A', 'A');
	char pairs[20]= {'A','T','D','F',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	setLetterPairs(pairs);
	intitialize();
    while(1) {
        //int getal = buttonsPressed();

        char temp1 = getEvent();
        if(temp1 == 0){
        	printf("empty\n");
        }
        else{
        	if(temp1 != '3'){
            	printf("%c",temp1);printf(" before encoding\n");
            	temp1 = codeer( temp1);
            	printf("%c",temp1);printf(" after encoding\n");
            	temp1 = codeer( temp1);
            	printf("%c",temp1);printf(" after decoding\n");
        	}
        	else{
        		printf("OK");
        	}
        }
        int dips = testRead();
    }
    return 0 ;
}
