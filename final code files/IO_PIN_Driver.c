/*
 * IO_PIN_Driver.c
 *
 *  Created on: 28 nov. 2018
 *      Author: Ian
 *      	Made the first version of the functions
 *      	Due to hardwareconnections version not usable
 *      co-Author: Jelle
 *      	Edited functions to work with hardware
 */

#include "IO_PIN_Driver.h"

/*
 * Initializes the functionality of the necessary pins on the mbed required for the PINs on the education board
 */
void initialize_pins(){
	LPC_PINCON->PINSEL0 &= ~(3<<18);
	LPC_PINCON->PINSEL0 &= ~(3<<16);
	LPC_PINCON->PINSEL0 &= ~(3<<14);
	LPC_PINCON->PINSEL0 &= ~(3<<12);
	LPC_PINCON->PINSEL0 &= ~(3<<0);
	LPC_PINCON->PINSEL0 &= ~(3<<2);
	LPC_PINCON->PINSEL1 &= ~(3<<4);
	LPC_PINCON->PINSEL1 &= ~(3<<2);
	LPC_PINCON->PINSEL0 &= ~(3<<30);
	LPC_PINCON->PINSEL1 &= ~(3<<0);
	LPC_PINCON->PINSEL1 &= ~(3<<14);
	LPC_PINCON->PINSEL1 &= ~(3<<16);
	LPC_PINCON->PINSEL1 &= ~(3<<18);
	LPC_PINCON->PINSEL1 &= ~(3<<20);
	LPC_PINCON->PINSEL3 &= ~(3<<28);
	LPC_PINCON->PINSEL3 &= ~(3<<30);
	LPC_PINCON->PINSEL4 &= ~(3<<10);
	LPC_PINCON->PINSEL4 &= ~(3<<8);
	LPC_PINCON->PINSEL4 &= ~(3<<6);
	LPC_PINCON->PINSEL4 &= ~(3<<4);
	LPC_PINCON->PINSEL4 &= ~(3<<2);
	LPC_PINCON->PINSEL4 &= ~(3<<0);
	LPC_PINCON->PINSEL0 &= ~(3<<22);
	LPC_PINCON->PINSEL0 &= ~(3<<20);
	LPC_PINCON->PINSEL0 &= ~(3<<10);
	LPC_PINCON->PINSEL0 &= ~(3<<8);
}

/*
 * Sets the directions of the initialized pins
 */
void direction_pins(){
	LPC_GPIO0->FIODIR = 0;
	LPC_GPIO1->FIODIR = 0;
	LPC_GPIO2->FIODIR = 0;
	LPC_GPIO0->FIODIR |= (1<<9);
	LPC_GPIO0->FIODIR |= (0<<8);
	LPC_GPIO0->FIODIR |= (1<<7);
	LPC_GPIO0->FIODIR |= (0<<6);
	LPC_GPIO0->FIODIR |= (1<<0);
	LPC_GPIO0->FIODIR |= (0<<1);
	LPC_GPIO0->FIODIR |= (1<<18);
	LPC_GPIO0->FIODIR |= (0<<17);
	LPC_GPIO0->FIODIR |= (1<<15);
	LPC_GPIO0->FIODIR |= (0<<16);
	LPC_GPIO0->FIODIR |= (1<<23);
	LPC_GPIO0->FIODIR |= (0<<24);
	LPC_GPIO0->FIODIR |= (1<<25);
	LPC_GPIO0->FIODIR |= (0<<26);
	LPC_GPIO1->FIODIR |= (1<<30);
	LPC_GPIO1->FIODIR |= (0<<31);
	LPC_GPIO2->FIODIR |= (1<<5);
	LPC_GPIO2->FIODIR |= (0<<4);
	LPC_GPIO2->FIODIR |= (1<<3);
	LPC_GPIO2->FIODIR |= (0<<2);
	LPC_GPIO2->FIODIR |= (1<<1);
	LPC_GPIO2->FIODIR |= (0<<0);
	LPC_GPIO0->FIODIR |= (1<<11);
	LPC_GPIO0->FIODIR |= (0<<10);
	LPC_GPIO0->FIODIR |= (1<<5);
	LPC_GPIO0->FIODIR |= (0<<4);
}

/*
 * Sets the required pins to a high state that will power other pins if connected via a jumper
 */
void setPins(){
	LPC_GPIO0->FIOSET |= (1<<9);
	LPC_GPIO0->FIOSET |= (1<<7);
	LPC_GPIO0->FIOSET |= (1<<0);
	LPC_GPIO0->FIOSET |= (1<<18);
	LPC_GPIO0->FIOSET |= (1<<15);
	LPC_GPIO0->FIOSET |= (1<<23);
	LPC_GPIO0->FIOSET |= (1<<25);
	LPC_GPIO1->FIOSET |= (1<<30);
	LPC_GPIO2->FIOSET |= (1<<5);
	LPC_GPIO2->FIOSET |= (1<<3);
	LPC_GPIO2->FIOSET |= (1<<1);
	LPC_GPIO0->FIOSET |= (1<<11);
	LPC_GPIO0->FIOSET |= (1<<5);
}

/*
 * Sets the pins set to high back to a low state
 */
void clearPins(){
	LPC_GPIO0->FIOCLR |= (1<<9);
	LPC_GPIO0->FIOCLR |= (1<<7);
	LPC_GPIO0->FIOCLR |= (1<<0);
	LPC_GPIO0->FIOCLR |= (1<<18);
	LPC_GPIO0->FIOCLR |= (1<<15);
	LPC_GPIO0->FIOCLR |= (1<<23);
	LPC_GPIO0->FIOCLR |= (1<<25);
	LPC_GPIO1->FIOCLR |= (1<<30);
	LPC_GPIO2->FIOCLR |= (1<<5);
	LPC_GPIO2->FIOCLR |= (1<<3);
	LPC_GPIO2->FIOCLR |= (1<<1);
	LPC_GPIO0->FIOCLR |= (1<<11);
	LPC_GPIO0->FIOCLR |= (1<<5);
}

/*
 * Sets the mask of the required GPIO's to only read the desired pins
 */
void mask(){
	LPC_GPIO0->FIOMASK=0;
	LPC_GPIO1->FIOMASK=0;
	LPC_GPIO2->FIOMASK=0;

	LPC_GPIO0->FIOMASK |= (1<<8);
	LPC_GPIO0->FIOMASK |= (1<<6);
	LPC_GPIO0->FIOMASK |= (1<<1);
	LPC_GPIO0->FIOMASK |= (1<<17);
	LPC_GPIO0->FIOMASK |= (1<<16);
	LPC_GPIO0->FIOMASK |= (1<<24);
	LPC_GPIO0->FIOMASK |= (1<<26);
	LPC_GPIO1->FIOMASK |= (1<<31);
	LPC_GPIO2->FIOMASK |= (1<<4);
	LPC_GPIO2->FIOMASK |= (1<<2);
	LPC_GPIO2->FIOMASK |= (1<<0);
	LPC_GPIO0->FIOMASK |= (1<<10);
	LPC_GPIO0->FIOMASK |= (1<<4);
	LPC_GPIO0->FIOMASK=~(LPC_GPIO0->FIOMASK);
	LPC_GPIO1->FIOMASK=~(LPC_GPIO1->FIOMASK);
	LPC_GPIO2->FIOMASK=~(LPC_GPIO2->FIOMASK);
}

/*
 * sets the pins that need to be read to a pull-down resistor
 */
void pinmode(){
	LPC_PINCON->PINMODE0 = 0x33330C;
	LPC_PINCON->PINMODE1 = 0x33000F;
	LPC_PINCON->PINMODE3 = 0xC0000000;
	LPC_PINCON->PINMODE4 = 0x333;
}

/*
 * Reads the pins from the GPIO's and places them in a 32bit int
 *
 * return: the integer with the values of the pins
 */
int readPins(){
	initialize_pins();
	direction_pins();
	setPins();
	int value_JP905 = ((LPC_GPIO0->FIOPIN>>8)&1) +(((LPC_GPIO0->FIOPIN>>6)&1)<<1) +(((LPC_GPIO0->FIOPIN>>1)&1)<<2) +(((LPC_GPIO0->FIOPIN>>17)&1)<<3);
	int value_JP906 = ((LPC_GPIO0->FIOPIN>>14)&1) +(((LPC_GPIO0->FIOPIN>>24)&1)<<1) +(((LPC_GPIO0->FIOPIN>>26)&1)<<2) +(((LPC_GPIO1->FIOPIN>>31)&1)<<3);
	int value_JP915 = ((LPC_GPIO2->FIOPIN>>4)&1) +(((LPC_GPIO2->FIOPIN>>2)&1)<<1) +(((LPC_GPIO2->FIOPIN>>0)&1)<<2) +(((LPC_GPIO0->FIOPIN>>10)&1)<<3);
	int value_JP916 = ((LPC_GPIO0->FIOPIN>>4)&1);
	int value_all_JP = (((((value_JP916<<4)+value_JP915)<<4)+value_JP906)<<4)+value_JP905;
	clearPins();
	return value_all_JP;
}

/*
 * Initializes the pins 5 and 6 from the mbed
 * 5 is set to output and 6 is set to input
 * Function reads the state of pin 6
 *
 * return: returns the value of pin 6.
 */
int testRead(){
	LPC_PINCON->PINSEL0 &= ~(3<<18);
	LPC_PINCON->PINSEL0 &= ~(3<<16);

	LPC_GPIO0->FIODIR = 0;
	LPC_GPIO0->FIODIR |= (1<<9);
	LPC_GPIO0->FIODIR |= (0<<8);
	LPC_PINCON->PINMODE0 = (3<<16);
	LPC_GPIO0->FIOCLR |= 0xFFFFFFF;
	LPC_GPIO0->FIOSET |= (1<<9);
	LPC_GPIO0->FIOMASK = ~(0x100);
	int temp = LPC_GPIO0->FIOPIN;
	LPC_GPIO0->FIOCLR |= (1<<9);

	int temp1= temp>>8;
	int temp2= temp1 & 1;
	//printf("%d temp  %d temp1  %d temp2",temp, temp1,temp2);
	//(LPC_GPIO0->FIOPIN>>8)&1;

	return temp2;
}
