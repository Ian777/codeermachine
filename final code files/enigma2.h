/*
 * enigma2.h
 *
 *  Created on: 16 nov. 2018
 *      Author: jelle
 */

#ifndef ENIGMA2_H_
#define ENIGMA2_H_

char codeer(char input);

void intitialize();
void setRotor1();
void setRotor2();
void setRotor3();
void rotateRotor(char rotor[2][26],int layer);
void copyArray(char array[2][26],int layer, const char array2[26]);

int* getOffset();
void setOffset(int offset1, int offset2, int offset3);
char* getStartPos();
void setStartPos(char start1, char start2, char start3);

void setLetterPairs(char pairs[20]);

void getCharFromRotor(char rotor[2][26], char* input,int* returnposition, int rotor_in, int rotor_out);
int getIntFromChar(char input);
char getCharFromInt(int input);
void setMaxLength(int length);
void codeerMessage(char* outputArray,const char* inputArray);

#endif /* ENIGMA2_H_ */
