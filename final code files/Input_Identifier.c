/*
 * Input_Identifier.c
 *
 *  Created on: 24 okt. 2018
 *      Author: jelle
 */


#include "Input_Identifier.h"

/*
 *  Determines which button is pressed and which pins are set to high
 *
 * @param array
 * 		array: array in which the the determend button and pins are placed
 */
void getIdentity(int* array){
	int button_value = buttonsPressed();
	int dip_value = testRead();
	array[1] = dip_value;
	int pressed_button = 0;
	for(int i=0; i<=26; i++){
		pressed_button = i;
		if((button_value & 0x1)==1){
			break;
		}
		else{
			button_value = button_value>>1;
		}
	}

	array[0] = pressed_button;
}
