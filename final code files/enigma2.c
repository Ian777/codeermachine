/*
 * enigma2.c
 *
 *  Created on: 16 nov. 2018
 *      Author: jelle
 */

#include "enigma2.h"
enum{upper = 0,lower = 1};

int offset[] = {0, 0 , 0}; //array containing the offset of the rotors
char startPos[] = {'A' , 'A', 'A'}; //array containing the startcharacter of the rotors
int maxLength; // determines the max length of  message

char IOarray[26]={'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};

char rotor1[2][26];
char rotor2[2][26];
char rotor3[2][26];
char letterPairs[2][26];
char reflec[2][26]={{'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'},
					{'Y','R','U','H','Q','S','L','D','P','X','N','G','O','K','M','I','E','B','F','Z','C','W','V','J','A','T'}};

/*
 * Takes a character and encodes it to another character
 *
 * @param: input
 * 		input: character that needs to be encoded
 *
 * returns: returns the encoded character
 */
char codeer(char input){
	char temp_char = input;
	int temp_int;

	getCharFromRotor(letterPairs , &temp_char, &temp_int, upper, lower);

	temp_int = getIntFromChar(temp_char);

	getCharFromRotor(rotor1, &temp_char, &temp_int, upper, lower);
	getCharFromRotor(rotor2, &temp_char, &temp_int, upper, lower);
	getCharFromRotor(rotor3, &temp_char, &temp_int, upper, lower);
	getCharFromRotor(reflec, &temp_char, &temp_int, upper, lower);
	getCharFromRotor(rotor3, &temp_char, &temp_int, lower, upper);
	getCharFromRotor(rotor2, &temp_char, &temp_int, lower, upper);
	getCharFromRotor(rotor1, &temp_char, &temp_int, lower, upper);

	//temp_char = getCharFromInt(temp_int);
	getCharFromRotor(letterPairs , &temp_char, &temp_int, lower, upper);

	return temp_char;
}


/*
 * Initializes the rotors
 */
void intitialize(){
	setRotor1();
	setRotor2();
	setRotor3();
}

/*
 * Initializes rotor 1 with corresponding offset and startcharacter
 */
void setRotor1(){
	char upperrotor[]  = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
	char lowerrotor1[] = {'E','K','M','F','L','G','D','Q','V','Z','N','T','O','W','Y','H','X','U','S','P','A','I','B','R','C','J'};
    //int offset = offset1;
    copyArray(rotor1,upper,upperrotor);
    copyArray(rotor1,lower,lowerrotor1);

	//set upper rotor 1
	for(; startPos[0] != rotor1[upper][0];offset[0]++){
		rotateRotor(rotor1,upper);
	}
	//set lower rotor 1
	for(int i = 0; i < offset[0];i++){
		rotateRotor(rotor1,lower);
	}

}

/*
 * Initializes rotor 2 with corresponding offset and startcharacter
 */
void setRotor2(){
	char upperrotor[]  = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
	char lowerrotor2[] = {'A','J','D','K','S','I','R','U','X','B','L','H','W','T','M','C','Q','G','Z','N','P','Y','F','V','O','E'};
    copyArray(rotor2,upper,upperrotor);
    copyArray(rotor2,lower,lowerrotor2);

	//set upper rotor 2
	for(; startPos[1] != rotor2[upper][0];offset[1]++){
		rotateRotor(rotor2,upper);
	}
	//set lower rotor 1
	for(int i = 0; i < offset[1];i++){
		rotateRotor(rotor2,lower);
	}

}

/*
 * Initializes rotor 3 with corresponding offset and startcharacter
 */
void setRotor3(){
	char upperrotor[]  = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
	char lowerrotor3[] = {'B','D','F','H','J','L','C','P','R','T','X','V','Z','N','Y','E','I','W','G','A','K','M','U','S','Q','O'};
    //int offset = offset3;
    copyArray(rotor3,upper,upperrotor);
    copyArray(rotor3,lower,lowerrotor3);

	//set upper rotor 3
	for(; startPos[2] != rotor3[upper][0];offset[2]++){
		rotateRotor(rotor3,upper);
	}
	//set lower rotor 3
	for(int i = 0; i < offset[2];i++){
		rotateRotor(rotor3,lower);
	}
}

/*
 * Makes it possible to shift a rotor one time
 *
 * @param: rotor[2][26] , layer
 * 		rotor[2][26]: the rotor that needs to be shifted
 * 		layer: which layer of the rotor that needs to be rotated
 */
void rotateRotor(char rotor[2][26],int layer){
	char temp = rotor[layer][0];
	for(int i = 0; i < 25;i++){
		rotor[layer][i] = rotor[layer][i+1];
	}
	rotor[layer][25] = temp;
}

/*
 * Makes it possible to copy an array to another array
 *
 * @param: array[2][26] , layer , array2[2][26]
 * 		array[2][26]: array which needs to be copied to
 * 		layer: which layer of the rotor that needs to be rotated
 * 		array2[2][26]: array which which will be copied to the other array
 */
void copyArray(char array[2][26],int layer, const char array2[26]){
	for(int i = 0; i < 26;i++){
		array[layer][i] = array2[i];
	}
}

/*
 * returns the offset
 *
 * returns: returns the array containing the offset of the rotors
 */
int* getOffset(){
	return offset;
}

/*
 * sets the offset into the array
 *
 * @param: offset1 , offset2 , offset3
 * 		offset1: the offset for rotor 1
 * 		offset2: the offset for rotor 2
 * 		offset3: the offset for rotor 3
 *
 */
void setOffset(int offset1, int offset2, int offset3){
	offset[0] = offset1;
	offset[1] = offset2;
	offset[2] = offset3;
}

/*
 * returns the start characters
 *
 * returns: returns the array containing the startcharacters of the rotors
 */
char* getStartPos(){
	return startPos;
}

/*
 * sets the startposition into the array
 *
 * @param: startt1 , start2 , start3
 * 		start1: the startposition for rotor 1
 * 		start2: the startposition for rotor 2
 * 		start3: the startposition for rotor 3
 *
 */
void setStartPos(char start1, char start2, char start3){
	startPos[0] = start1;
	startPos[1] = start2;
	startPos[2] = start3;
}

/*
 * Sets the letterpairs in the array
 *
 * @param: pairs[20]
 * 		pairs[20]: contains the letterpairs
 *
 */
void setLetterPairs(char pairs[20]){
	char layer[] = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
	copyArray(letterPairs,0,layer);
	copyArray(letterPairs,1,layer);
	for(int i = 0;i<20 ; i+=2){
		if(pairs[i] != 0){
			for(int j = 0; j<26 ;j++){
				if(pairs[i] == letterPairs[0][j]){
					letterPairs[1][j] = pairs[1+1];
					break;
				}
			}
		}
		else{
			break;
		}
	}
}

/*
 * gets a char from a rotor to get an encodes char from it
 *
 * @param: rotor[2][26] , input , returnposition , rotor_in , rotor_out
 * 		rotor[2][26]: the rotor where you need to get the char from
 * 		input: the pointer in which the final char will be placed
 * 		returnposition: the pointer in which the position will be placed
 * 		rotor_in: the layer which the char goes in to determine the position for the other rotorlayer
 * 		rotor_out: the layer which will give the encoded char
 */
void getCharFromRotor(char rotor[2][26], char* input, int* returnposition,int rotor_in, int rotor_out){
	for(int i = 0;i<26;i++){
		if(*input == rotor[rotor_in][i]){
			*input =  rotor[rotor_out][i];
			break;
		}
	}
	char temp = rotor[rotor_out][*returnposition];
	for(int i = 0;i<26;i++){
		if(temp == rotor[rotor_in][i]){
			*returnposition = i;
			break;
		}
	}
}

/*
 * gets the position of a char in the IOarray
 *
 * @param: input
 * 		input: the character you want to get the position from
 *
 * returns: returns the position
 */
int getIntFromChar(char input){
	for(int i=0;i<26;i++){
		if(input != IOarray[i]){
			return i;
			break;
		}
	}
	return 0;
}

/*
 * gets the char of the position in the IOarray
 *
 * @param: input
 * 		input: the position you want to get the character from
 *
 * returns: returns the char
 */
char getCharFromInt(int input){
	return IOarray[input];
}

/*char swap(char input, char* letterParen)
{
	char resultaat=input;
	for(int i=0;i<20;i++)
	{
		if((letterParen[i]==resultaat) && (i%2==0))
		{
			resultaat = letterParen[i+1];
			break;
		}
		else if((letterParen[i]==resultaat )&& (i%2!=0))
		{
			resultaat = letterParen[i-1];
			break;
		}
	}
	return resultaat;
}*/

/*
 * sets the max length
 *
 * @param: length
 * 		length: the integer for the max length
 */
void setMaxLength(int length){
	maxLength = length;
}

/*
 * Encodes an entire message from a character array
 *
 * @param: outputArray[maxLength] , inputArray[maxLength]
 * 		outputArray[maxLength]: the array in which the encoded message will be placed
 * 		inputArray[maxLength]: the array which contains the message that needs to be encoded
 */
void codeerMessage(char outputArray[maxLength],const char inputArray[maxLength]){
	for(int i = 0; i<maxLength ;i++){
		if(inputArray[i] =='\0'){
			outputArray[i] = inputArray[i];
			break;
		}
		else{
			outputArray[i] = codeer(inputArray[i]);
		}
	}
}
