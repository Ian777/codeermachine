#ifndef LCD_DRIVER_H
#define LCD_DRIVER_H

#include "LPC17xx.h"

/*
 * methode dat de chips van de lcd juist instelt.
 */
void setChips();

/*
 * methode dat de lcd scherm blanco maakt.
 */
void resetLCD();

/*
 * methode om de byte op de data bus te plaatsen
 * @params de byte die op de data bus moet komen
 */
void sendByte(uint8_t teVerzByte);

/*
 * methode dat een clock maakt voor de display bus
 */
void dataClock();

/*
 * methode dat de pins instelt om te communiceren met de lcd driver chips
 */
void initPins();

/*
 * methode dat ervoor zorgt dat de er karakters op de lcd getoond worden
 * @params int chip: selecteert linker of rechter chip elke chip bestaat uit 64 koloms
 * @params unit8_t data: de bit dat er getoont moet worden.
 */
void sendData(int chip, uint8_t data);

/*
 * methode om commandos te sturen naar een chip
 * @param uint8_t data: het commando
 */
void sendCommand(uint8_t data);

#endif
