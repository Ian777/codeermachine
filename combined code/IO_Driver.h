#ifndef IO_DRIVER_H
#define IO_DRIVER_H

/*
 *methode dat in een int de positie van alle dip switches van de IO gedeelte teruggeeft
 *@return de integer met alle posities van de dipswitch elke bit komt overeen met switch
 */
int dipPos();

/*
 * methode dat de benodige pins in GPIO mode zet
 */
void selectPinsIODip();

/*
 * methode dat de benodigde pins in input zet
 */
void IOPinsDirection();

/*
 * methode dat ervoor zorgt dat de pins uitgelezen kunnen worden door de cpu
 */
void IOPinMask();

#endif
