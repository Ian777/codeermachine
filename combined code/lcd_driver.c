#include "LPC17xx.h"
#include "LCD_DRIVER.h"

void setChips()
{
	initPins();
	LPC_GPIO1->FIOSET |= (0b1 <<30);
	LPC_GPIO1->FIOCLR |= (0b1 <<31);
	LPC_GPIO1->FIOCLR |= (0b1 <<5);
	LPC_GPIO1->FIOSET |= (0b1 <<4);
	sendCommand(0xC0);
	sendCommand(0x40);
	sendCommand(0xB8);
	sendCommand(0x3F);
}

void sendCommand(uint8_t command)
{
	LPC_GPIO1->FIOCLR |= (0b1 <<30);
	LPC_GPIO1->FIOCLR |= (0b1 <<31);
	LPC_GPIO1->FIOCLR |= (0b1 <<5);
	LPC_GPIO1->FIOSET |= (0b1 <<4);
	sendByte(command);
	LPC_GPIO1->FIOSET |= (0b1 <<30);
	LPC_GPIO1->FIOCLR |= (0b1 <<30);
}

void sendData(int chip, uint8_t data)
{
	if(chip==1)
	{
		LPC_GPIO1->FIOCLR |= (0b1 <<30);
		LPC_GPIO1->FIOSET |= (0b1 <<31);
		LPC_GPIO1->FIOCLR |= (0b1 <<5);
		LPC_GPIO1->FIOSET |= (0b1 <<4);
		sendByte(data);
		LPC_GPIO1->FIOSET |= (0b1 <<30);
		LPC_GPIO1->FIOCLR |= (0b1 <<30);
	}
	else
	{
		LPC_GPIO1->FIOCLR |= (0b1 <<30);
		LPC_GPIO1->FIOCLR |= (0b1 <<31);
		LPC_GPIO1->FIOSET |= (0b1 <<5);
		LPC_GPIO1->FIOSET |= (0b1 <<4);
		sendByte(data);
		LPC_GPIO1->FIOSET |= (0b1 <<30);
		LPC_GPIO1->FIOCLR |= (0b1 <<30);
	}
}

void initPins()
{
	LPC_PINCON->PINSEL1 &= ~(0b11<<14);//0.23 strobe
	LPC_PINCON->PINSEL1 &= ~(0b11<<16);//0.24 data
	LPC_PINCON->PINSEL1 &= ~(0b11<<18);//0.25 clock
	LPC_PINCON->PINSEL1 &= ~(0b11<<20);//0.26 output enable
	LPC_PINCON->PINSEL3 &= ~(0b11<<28);//1.30 A0
	LPC_PINCON->PINSEL3 &= ~(0b11<<30);//1.31 A1
	LPC_PINCON->PINSEL4 &= ~(0b11<<10);//2.5 A2
	LPC_PINCON->PINSEL4 &= ~(0b11<<8);//2.4 A3

	LPC_GPIO0->FIODIR |= (0b1<<23);
	LPC_GPIO0->FIODIR |= (0b1<<24);
	LPC_GPIO0->FIODIR |= (0b1<<25);
	LPC_GPIO0->FIODIR |= (0b1<<26);
	LPC_GPIO1->FIODIR |= (0b1<<30);
	LPC_GPIO3->FIODIR |= (0b1<<31);
	LPC_GPIO2->FIODIR |= (0b1<<5);
	LPC_GPIO2->FIODIR |= (0b1<<4);

	LPC_GPIO0->FIOMASK &= ~(0b1<<23);
	LPC_GPIO0->FIOMASK &= ~(0b1<<24);
	LPC_GPIO0->FIOMASK &= ~(0b1<<25);
	LPC_GPIO0->FIOMASK &= ~(0b1<<26);
	LPC_GPIO1->FIOMASK &= ~(0b1<<30);
	LPC_GPIO1->FIOMASK &= ~(0b1<<31);
	LPC_GPIO2->FIOMASK &= ~(0b1<<5);
	LPC_GPIO2->FIOMASK &= ~(0b1<<4);
}

void resetLCD()
{
	initPins();
	LPC_GPIO1->FIOCLR |= (0b1 <<30);
	LPC_GPIO1->FIOSET |= (0b1 <<31);
	LPC_GPIO1->FIOSET |= (0b1 <<5);
	LPC_GPIO1->FIOSET |= (0b1 <<4);
}

void sendByte(uint8_t teVerzByte)
{
	LPC_GPIO0->FIOSET |= (0b1<<26);
	LPC_GPIO0->FIOCLR |= (0b1<<23);
	for(int i =7;i<0;i--)
	{
		if((teVerzByte&(0b1<<i)) ==0)
		{
			LPC_GPIO0->FIOCLR |= (0b1<<24);
			dataClock();
		}
		else
		{
			LPC_GPIO0->FIOSET |= (0b1<<24);
			dataClock();
		}
	}
	LPC_GPIO0->FIOSET |= (0b1<<23);
	dataClock();
	LPC_GPIO0->FIOCLR |= (0b1<<23);
}

void dataClock()
{
	LPC_GPIO0->FIOSET |= (0b1<<25);
	LPC_GPIO0->FIOCLR |= (0b1<<25);
}
