#include"lcd_controller.h"
#include "lcd_driver.h"

void printBericht(char* bericht, int length)
{
	resetLCD();
	setChips();
	char x;
	for(int i=0;i<length;i++ )
	{
		x=bericht[i];
		if(counter()==64)
		{
			LCDRight();
			determinePrint(x);
		}
		else
		{
			LCDLeft();
			determinePrint(x);
		}
	}
}

void determinePrint(char x)
{
	switch(x)
		{
			case '_':
				printBlank();
				break;
			case '#':
				printSelector();
				break;
			case 'A':
				printA();
				break;
			case 'B':
				printB();
				break;
			case 'C':
				printC();
				break;
			case 'D':
				printD();
				break;
			case 'E':
				printE();
				break;
			case 'F':
				printF();
				break;
			case 'G':
				printG();
				break;
			case 'H':
				printH();
				break;
			case 'I':
				printI();
				break;
			case 'J':
				printJ();
				break;
			case 'K':
				printK();
				break;
			case 'L':
				printL();
				break;
			case 'M':
				printM();
				break;
			case 'N':
				printN();
				break;
			case 'O':
				printO();
				break;
			case 'P':
				printP();
				break;
			case 'Q':
				printQ();
				break;
			case 'R':
				printR();
				break;
			case 'S':
				printS();
				break;
			case 'T':
				printT();
				break;
			case 'U':
				printU();
				break;
			case 'W':
				printW();
				break;
			case 'X':
				printX();
				break;
			case 'Y':
				printY();
				break;
			case 'Z':
				printZ();
				break;
			default:
				printBlank();
		}
}

int counter()
{
	static int counter= 0;
	counter++;
	if(counter==128)
	{
		counter=0;
	}
	return counter;
}


void printBlank()
{
	sendByte(0x03);
	sendByte(0x03);
	sendByte(0x03);
	sendByte(0x03);
}

void printSelector()
{
	sendByte(0x6C);
	sendByte(0xFF);
	sendByte(0x6C);
	sendByte(0xFF);
}

void printA()
{
	sendByte(0x00);
	sendByte(0x3F);
	sendByte(0x48);
	sendByte(0x3F);
}

void printB()
{
	sendByte(0x00);
	sendByte(0x7F);
	sendByte(0x49);
	sendByte(0x36);
}

void printC()
{
	sendByte(0x00);
	sendByte(0x1C);
	sendByte(0x22);
	sendByte(0x41);
}

void printD()
{
	sendByte(0x00);
	sendByte(0x7F);
	sendByte(0x41);
	sendByte(0x3E);
}

void printE()
{
	sendByte(0x00);
	sendByte(0x7F);
	sendByte(0x4D);
	sendByte(0x4D);
}

void printF()
{
	sendByte(0x00);
	sendByte(0x7F);
	sendByte(0x58);
	sendByte(0x58);
}

void printG()
{
	sendByte(0x00);
	sendByte(0x3E);
	sendByte(0x61);
	sendByte(0x46);
}

void printH()
{
	sendByte(0x00);
	sendByte(0x7F);
	sendByte(0x0C);
	sendByte(0x7F);
}

void printI()
{
	sendByte(0x00);
	sendByte(0x63);
	sendByte(0x7F);
	sendByte(0x63);
}

void printJ()
{
	sendByte(0x00);
	sendByte(0x46);
	sendByte(0x41);
	sendByte(0x7E);
}

void printK()
{
	sendByte(0x00);
	sendByte(0x7F);
	sendByte(0x12);
	sendByte(0x21);
}

void printL()
{
	sendByte(0x00);
	sendByte(0x7F);
	sendByte(0x03);
	sendByte(0x03);
}

void printM()
{
	sendByte(0x00);
	sendByte(0x7F);
	sendByte(0x20);
	sendByte(0xFE);
}

void printN()
{
	sendByte(0x00);
	sendByte(0x7F);
	sendByte(0x40);
	sendByte(0x7F);
}

void printO()
{
	sendByte(0x00);
	sendByte(0x3E);
	sendByte(0x41);
	sendByte(0x3E);
}

void printP()
{
	sendByte(0x00);
	sendByte(0x7F);
	sendByte(0x48);
	sendByte(0x30);
}

void printQ()
{
	sendByte(0x00);
	sendByte(0x1C);
	sendByte(0x22);
	sendByte(0x1D);
}

void printR()
{
	sendByte(0x00);
	sendByte(0x7F);
	sendByte(0x42);
	sendByte(0x3D);
}

void printS()
{
	sendByte(0x00);
	sendByte(0x09);
	sendByte(0x15);
	sendByte(0x12);
}

void printT()
{
	sendByte(0x00);
	sendByte(0x60);
	sendByte(0x7F);
	sendByte(0x60);
}

void printU()
{
	sendByte(0x00);
	sendByte(0x7E);
	sendByte(0x01);
	sendByte(0x7F);
}

void printV()
{
	sendByte(0x00);
	sendByte(0x7C);
	sendByte(0x02);
	sendByte(0x7C);
}

void printW()
{
	sendByte(0x00);
	sendByte(0x7F);
	sendByte(0x02);
	sendByte(0x7F);
}

void printX()
{
	sendByte(0x00);
	sendByte(0x77);
	sendByte(0x08);
	sendByte(0x77);
}

void printY()
{
	sendByte(0x00);
	sendByte(0x71);
	sendByte(0x09);
	sendByte(0x7E);
}

void printZ()
{
	sendByte(0x00);
	sendByte(0x67);
	sendByte(0x6B);
	sendByte(0x73);
}

