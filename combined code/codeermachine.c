/*
===============================================================================
 Name        : codeermachine.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>
#include "lcd_driver.h"
#include "lcd_controller.h"
#include "statemachine.h"
#include "enigma2.h"
#include "LPC17xx.h"
#include "Input_Event.h"
#include "cryptoInstellingen.h"
#include "lcd_driver.h"

int main(void) {
    // Enter an infinite loop, running the statemachine
	while(1) {
	//	runStatemachine();
    }
    return 0 ;
}
