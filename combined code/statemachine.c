#include "statemachine.h"
#include "cryptoInstellingen.h"
#include "enigma2.h"
#include <stdio.h>

int charToNumber()
{
	int check = 1;
	while(check)
	{
		switch(validateEvent())
		{
			case 'A':
				printf("keuze 0\n\r");
				return 0;
				break;
			case 'B':
				printf("keuze 1\n\r");
				return 1;
				break;
			case 'C':
				printf("keuze 2\n\r");
				return 2;
				break;
			case 'D':
				printf("keuze 3\n\r");
				return 3;
				break;
			case 'E':
				printf("keuze 4\n\r");
				return 4;
				break;
			case 'F':
				printf("keuze 5\n\r");
				return 5;
				break;
			case 'G':
				printf("keuze 6\n\r");
				return 6;
				break;
			case 'H':
				printf("keuze 7\n\r");
				return 7;
				break;
			case 'I':
				printf("keuze 8\n\r");
				return 8;
				break;
			case 'J':
				printf("keuze 9\n\r");
				return 9;
				break;
			case 'K':
				printf("keuze 10\n\r");
				return 10;
				break;
			case 'L':
				printf("keuze 11\n\r");
				return 11;
				break;
			case 'M':
				printf("keuze 12\n\r");
				return 12;
				break;
			case 'N':
				printf("keuze 13\n\r");
				return 13;
				break;
			case 'O':
				printf("keuze 14\n\r");
				return 14;
				break;
			case 'P':
				printf("keuze 15\n\r");
				return 15;
				break;
			case 'Q':
				printf("keuze 16\n\r");
				return 16;
				break;
			case 'R':
				printf("keuze 17\n\r");
				return 17;
				break;
			case 'S':
				printf("keuze 18\n\r");
				return 18;
				break;
			case 'T':
				printf("keuze 19\n\r");
				return 19;
				break;
			case 'U':
				printf("keuze 20\n\r");
				return 20;
				break;
			case 'V':
				printf("keuze 21\n\r");
				return 21;
				break;
			case 'W':
				printf("keuze 22\n\r");
				return 22;
				break;
			case 'X':
				printf("keuze 23\n\r");
				return 23;
				break;
			case 'Y':
				printf("keuze 24\n\r");
				return 24;
				break;
			case 'Z':
				printf("keuze 25\n\r");
				return 25;
				break;
			default:
				return 0;
		}
	}
	return 0;
}
char validateEvent()
{
	int check = 1;
	while(check)
	{
		char* eventChars = getEvent();//steek het nulde element van de eventchar pointer in de var eventchar
		switch(eventChars[0])
		{
			case 'A':
				printf("keuze A\n\r");
				return 'A';
				break;
			case 'B':
				printf("keuze B\n\r");
				return 'B';
				break;
			case 'C':
				printf("keuze C\n\r");
				return 'C';
				break;
			case 'D':
				printf("keuze D\n\r");
				return 'D';
				break;
			case 'E':
				printf("keuze E\n\r");
				return 'E';
				break;
			case 'F':
				printf("keuze F\n\r");
				return 'F';
				break;
			case 'G':
				printf("keuze G\n\r");
				return 'G';
				break;
			case 'H':
				printf("keuze H\n\r");
				return 'H';
				break;
			case 'I':
				printf("keuze I\n\r");
				return 'I';
				break;
			case 'J':
				printf("keuze J\n\r");
				return 'J';
				break;
			case 'K':
				printf("keuze K\n\r");
				return 'K';
				break;
			case 'L':
				printf("keuze L\n\r");
				return 'L';
				break;
			case 'M':
				printf("keuze M\n\r");
				return 'M';
				break;
			case 'N':
				printf("keuze N\n\r");
				return 'N';
				break;
			case 'O':
				printf("keuze O\n\r");
				return 'O';
				break;
			case 'P':
				printf("keuze P\n\r");
				return 'P';
				break;
			case 'Q':
				printf("keuze Q\n\r");
				return 'Q';
				break;
			case 'R':
				printf("keuze R\n\r");
				return 'R';
				break;
			case 'S':
				printf("keuze S\n\r");
				return 'S';
				break;
			case 'T':
				printf("keuze T\n\r");
				return 'T';
				break;
			case 'U':
				printf("keuze U\n\r");
				return 'U';
				break;
			case 'V':
				printf("keuze V\n\r");
				return 'V';
				break;
			case 'W':
				printf("keuze W\n\r");
				return 'W';
				break;
			case 'X':
				printf("keuze X\n\r");
				return 'X';
				break;
			case 'Y':
				printf("keuze Y\n\r");
				return 'Y';
				break;
			case 'Z':
				printf("keuze Z\n\r");
				return 'Z';
				break;
			default:
				break;
		}
	}
	return 'A';
}


void runCryptoInstellingen()
{
	int check = 1;
	while(check)
	{
		printf("instellingen voor de codeermachine\n\r");
		printf("offset instellen(A)\n\r");
		printf("letterparen instellen(B)\n\r");
		printf("maximum aantal karakters(C)\n\r");
		printf("start karakters instellen(D)\n\r");
		printf("verlaat crypto instellingen(E)\n\r");
		int offsetSettings[3];
		char letterParen[20];
		char startKarakters[3];
		int aantalLetterparen;
		char keuze = validateEvent();
		if(keuze=='A')
		{
				printf("stel de offset in voor de drie rotors(A=0,Z=25)\n\r");
				printf("stel de eerste offset in(A=0,Z=25)\n\r");
				offsetSettings[0]=charToNumber();
				printf("stel de tweede offset in(A=0,Z=25)\n\r");
				offsetSettings[1]=charToNumber();
				printf("stel de derde offset in(A=0,Z=25)\n\r");
				offsetSettings[2]=charToNumber();
				setOffsetInstelling(offsetSettings);
		}
		else if(keuze=='B')
		{
				int maximumBeveiliging=1;
				while(maximumBeveiliging==1)
				{
					printf("hoeveel Letterparen wilt u, maximum is 10(A=0,K=10)\n\r");
					aantalLetterparen = charToNumber();
					if((aantalLetterparen*2)<=20)
					{
						maximumBeveiliging=0;
					}
					else
					{
						printf("geef een waarde in tussen 0 en 1O\n\r");
					}
				}
				for(int i =0;i<20;i++)
				{
					if(i<=(aantalLetterparen*2))
					{
						if(i%2==0)
						{
							printf("geef het karakter (%i) dat je wil vervangen\n\r",i);
							letterParen[i]=validateEvent();
						}
						else if(i%2!=0)
						{
							printf("geef het karakter waarmee het vervangen moet worden\n\r");
							letterParen[i]=validateEvent();
						}
					}
					else
					{
						letterParen[i]='1';
					}
				}
				setLetterpaar(letterParen);
		}
		else if(keuze=='C')
		{
				printf("geef het aantal cijfers in\n\r");
				int limiet = charToNumber();
				int waarde=0;
				for(int i =0;i<limiet;i++)
                {
                    printf("geef de waarde van cijfer %i :(A=0,Z=10)",i);
                    if(i!=0)
                    {
                        waarde+=charToNumber()*10*i;
                    }
                    else
                    {
                        waarde=charToNumber();
                    }
                }
				setMaxChar(waarde);
		}
		else if(keuze=='D')
		{
				printf("stell de start karakters in\n\r");
				printf("stel het eerste startkarakter in\n\r");
				startKarakters[0]=validateEvent();
				printf("stel het tweede startkarakter in\n\r");
				startKarakters[1]=validateEvent();
				printf("stel het derde startkarakter in\n\r");
				startKarakters[2]=validateEvent();
				setStartCharacters(startKarakters);
		}
		else if(keuze=='E')
		{
				check=0;
		}
	}
}

void runCoderen()
{
	int* offsetrotors=getOffSetRotors();
	char* letterParen = getLetterpaar();
	int maxChar = (int)*(getMaxChar());
	char*startcharacters = getStartCharacters();
	setOffset(offsetrotors[0], offsetrotors[1], offsetrotors[2]);
	setStartPos(startcharacters[0], startcharacters[1], startcharacters[2]);
	setLetterPairs(letterParen);
	setMaxLength(maxChar);
	intitialize();
	char bericht[(maxChar+2)];
	char input;
	for(int i =0;i<maxChar-1;i++)
	{
	    printf("geef een character\n\r");
		input=codeer(validateEvent());
	    printf("output: %c\n\r",input);
		bericht[i]=input;
	}
	bericht[maxChar]='\0';
	printf(bericht);
}

void runCodeerBericht()
{
	int* offsetrotors=getOffSetRotors();
	char* letterParen = getLetterpaar();
	int maxChar = (int)*(getMaxChar());
	char*startcharacters = getStartCharacters();
	setOffset(offsetrotors[0], offsetrotors[1], offsetrotors[2]);
	setStartPos(startcharacters[0], startcharacters[1], startcharacters[2]);
	setLetterPairs(letterParen);
	setMaxLength(maxChar);
	intitialize();
	char inputbericht[(maxChar)];
	char outputbericht[maxChar];
	char input;
	for(int i =0;i<maxChar;i++)
	{
	    printf("geef een character\n\r");
		input=(validateEvent());
	    printf("output: %c\n\r",input);
		inputbericht[i]=input;
	}
	printf(inputbericht+"\n\r");
	codeerMessage(outputbericht, inputbericht);
	printf(outputbericht+"\n\r");
}

void runInit()
{
	int offsetArray[3];
	offsetArray[0]=0;
	offsetArray[1]=0;
	offsetArray[2]=0;
	setOffsetInstelling(offsetArray);
	char letterPaarArray[20];
	for(int i = 0;i<20;i++)
	{
		letterPaarArray[i]=0;
	}
	setLetterpaar(letterPaarArray);
	setMaxChar(5);
	char startCharacters[3];
	startCharacters[0]='A';
	startCharacters[1]='A';
	startCharacters[2]='A';
	setStartCharacters(startCharacters);
}

void runStatemachine()
{
	int state=-1;
	runInit();
	while(1)
	{
		switch(state)
		{
			case -1:
				printf("druk op A om de encryptie instellingen te wijzingen\n\r");
				printf("druk op B om een bericht karakter per karakter te encoderen\n\r");
				printf("druk op C om een bericht karakter per karakter te decoderen\n\r");
				printf("druk op D om een bericht in te typen en dan te coderen\n\r");
				printf("druk op E om een bericht in te typen en dan te decoderen\n\r");
				state=charToNumber();
				break;
			case 0:
				printf("cryptoinstellingen:\n\r");
				runCryptoInstellingen();
				state = -1;
				break;
			case 1:
				printf("encodeer:\n\r");
				runCoderen();
				state=-1;
				break;
			case 2:
				printf("decodeer:\n\r");
				runCoderen();
				state=-1;
				break;
			case 3:
				printf("codeer bericht:\n\r");
				runCodeerBericht();
				state=-1;
				break;
			case 4:
				printf("decodeer bericht:\n\r");
				runCodeerBericht();
				state=-1;
				break;
		}
	}
}
