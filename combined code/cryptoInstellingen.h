/**
 *H file dat alle methodes bevat die zorgen dat de codeermachine ingesteld kan worden en gelezen worden.
 */
#ifndef CRYPTOINSTELLINGEN_H
#define CRYPTOINSTELLINGEN_H

#include "lpc17xx.h"

/*
 * Methode om de Offset van de rotors in te stellen.
 * @params int* settOffsetArray: Een array van drie integers die de offset bepalen.
 */
void setOffsetInstelling(int* setOffsetArray);

/*
 * Methode om de leterparen in te stellen.Deze karakters worden vervangen door hun buur. De leters op een even index door de letter recht van de even letter.
 * De letters op een oneven index door de letter links van de letter.
 * @params char* setLetterpaarArray: Een array van 20 karakters geindexeerd in paren van twee.Een even en oneven karakter.
 */
void setLetterpaar(char* setLetterpaarArray);

/*
 * Methode om het maximaal aantal karakters waaruit een bericht bestaat in te stellen.
 * @params int setMaxChar: het maximum aantal karakters
 */
void setMaxChar(int setMaxChar);

/*
 * methode die alle start karakters voor de rotors zet
 * @param char* setStartCharacters: de pointer naar de array met alle letters
 */
void setStartCharacters(char* setStartCharacters);

/*
 * methode die de pointer naar de array waar de offset van de rotors bewaardt worden
 * @return int*: pointer naar de array met de offsets
 */
int* getOffSetRotors();

/*
 * methode die de pointer naar de array die de letterparen bevat terruggeeft
 * @return de pointer naar de letterparen array
 */
char* getLetterpaar();

/*
 * methode die de pointer naar de maxChar teruggeeft.
 * @return de pointer naar de maxChar
 */
int* getMaxChar();

/*
 * methode die de pointer teruggeeft naar de array met de start karakters van de drie rotors
 * @return de pointer naar de array met de start karakters van de drie rotors
 */
char* getStartCharacters();
#endif

