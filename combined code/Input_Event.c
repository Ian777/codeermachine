/*
 * Input_Event.c
 *
 *  Created on: 30 okt. 2018
 *      Author: jelle
 */

#include "Input_Event.h"

char* getEvent(){
	int inputsarray[4] = {0, 0, 0, 0};
	getIdentity(inputsarray);
	//printf("%d button in inputevent\n",inputsarray[0]);
	//if(inputsarray[1]!=0){
		//return decideDip(inputsarray[0],0, inputsarray[1]);
		return decideChar(inputsarray[0]);
	//}
	/*else if(inputsarray[2]!=0){
		return "null";
	}
	else if(inputsarray[3]!=0){
		return "null";
	}
	else{
		return "null";
	}*/
}

char* decideChar(int button){
	//printf("%d",button);printf("in input event\n");
	switch(button){
	case 0:  return "O"; break;
	case 1:  return "R"; break;
	case 2:  return "H"; break;
	case 3:  return "D"; break;
	case 4:  return "L"; break;
	case 5:  return "I"; break;
	case 6:  return "E"; break;
	case 7:  return "A"; break;
	case 8:  return "Z"; break;
	case 9:  return "W"; break;
	case 10: return "B"; break;
	case 11: return "F"; break;
	case 12: return "X"; break;
	case 13: return "U"; break;
	case 14: return "J"; break;
	case 15: return "M"; break;
	case 16: return "P"; break;
	case 17: return "S"; break;
	case 18: return "V"; break;
	case 19: return "Y"; break;
	case 20: return "T"; break;
	case 21: return "Q"; break;
	case 22: return "N"; break;
	case 23: return "K"; break;
	case 24: return "G"; break;
	case 25: return "C"; break;
	default: return "nullcharacter";break;
	}
}

char* decideDip(int button, int dipswitch, int dipvalue){
	char* eventHolder;

	switch(dipswitch){
	case 0: switch(dipvalue){
			case 0: eventHolder = decideChar(button); break;
			case 1: eventHolder = decideMenubutton(button); break;
			case (1<<1):
			case (1<<2):
			case (1<<3):
			case (1<<4):
			case (1<<5):
			case (1<<6):
			case (1<<7):
			default: eventHolder = "nulldip0";break;
			}
			break;
	case 1: switch(dipvalue){
			case 0:
			case 1:
			case (1<<1):
			case (1<<2):
			case (1<<3):
			case (1<<4):
			case (1<<5):
			case (1<<6):
			case (1<<7):
			default: eventHolder = "null";break;
			}
			break;
	case 2: switch(dipvalue){
			case 0:
			case 1:
			case (1<<1):
			case (1<<2):
			case (1<<3):
			case (1<<4):
			case (1<<5):
			case (1<<6):
			case (1<<7):
			default: eventHolder = "null";break;
			}
			break;
	}
	return eventHolder;
}

char* decideMenubutton(int button){
	switch(button){
	case 10: return "up";break;
	case 14: return "down";break;
	default: return "nulldirection"; break;
	}
}
