#include "UART.H"


void setUpUART()
{
	LPC_UART0->LCR |= (1<<7);
	LPC_UART0->DLL = 104;
	LPC_UART0->DLM = 1;
	LPC_UART0->FDR = 1;
	LPC_UART0->FDR = (0b10<<4);
	LPC_UART0->LCR = 0b11;
	LPC_UART0->LCR &=~(1<<7);
	LPC_UART0->FCR |= 1;
	LPC_PINCON->PINSEL0 |= (5 << 4);
	LPC_PINCON->PINMODE0 |= (1<<7);
}

char getUARTCharacter()
{
	while((LPC_UART0->LSR & 1)==0);
	char received=LPC_UART0->RBR;
	return received;
}

void putUARTCharacter(char input)
{
	LPC_UART0->THR = input;
	while((LPC_UART0->LSR & (1<<6) )==0);
}
