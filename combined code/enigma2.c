/*
 * enigma2.c
 *
 *  Created on: 16 nov. 2018
 *      Author: jelle
 */

#include "enigma2.h"
enum{upper = 0,lower = 2};
char rotor1[2][26];/*={
	   	   	   	{"ABCDEFGHIJKLMNOPQRSTUVWXYZ"},
				{"EKMFLGDQVZNTOWYHXUSPAIBRCJ"}
				}*/
char rotor2[2][26];/*={
				{"ABCDEFGHIJKLMNOPQRSTUVWXYZ"},
				{"AJDKSIRUXBLHWTMCQGZNPYFVOE"}
				}*/
char rotor3[2][26];/*={
				{"ABCDEFGHIJKLMNOPQRSTUVWXYZ"},
				{"BDFHJLCPRTXVZNYEIWGAKMUSQO"}
				}*/
char reflec[2][26]={{'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'},
					 {'Y','R','U','H','Q','S','L','D','P','X','N','G','O','K','M','I','E','B','F','Z','C','W','V','J','A','T'}};

char codeer(char input){
	printf("%c",input);
	printf("in codeer methode\n");
	char temp_char= '0';
	for(int i = 0;i<26;i++){
		if(input == rotor1[0][i]){
			temp_char = rotor1[1][i];
			break;
		}
	}
	for(int i = 0;i<26;i++){
		if(temp_char == rotor2[0][i]){
			temp_char = rotor2[1][i];
			break;
		}
	}
	for(int i = 0;i<26;i++){
		if(temp_char == rotor3[0][i]){
			temp_char = rotor3[1][i];
			break;
		}
	}

	for(int i = 0;i<26;i++){
		if(temp_char == reflec[0][i]){
			temp_char = reflec[1][i];
			break;
		}
	}

	for(int i = 0;i<26;i++){
		if(temp_char == rotor3[1][i]){
			temp_char = rotor3[0][i];
			break;
		}
	}
	for(int i = 0;i<26;i++){
		if(temp_char == rotor2[1][i]){
			temp_char = rotor2[0][i];
			break;
		}
	}
	for(int i = 0;i<26;i++){
		if(temp_char == rotor1[1][i]){
			temp_char = rotor1[0][i];
			break;
		}
	}
	return temp_char;
}

void initialize(char start1, int offset1, char start2, int offset2, char start3, int offset3){
	setRotor1(start1, offset1);
	setRotor2(start2, offset2);
	setRotor3(start3, offset3);
}

void setRotor1(char start1, int offset1){
	char upperrotor[]  = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
	char lowerrotor1[] = {'E','K','M','F','L','G','D','Q','V','Z','N','T','O','W','Y','H','X','U','S','P','A','I','B','R','C','J'};
    int offset = offset1;
    copyArray(rotor1,upper,upperrotor);
    copyArray(rotor1,upper,lowerrotor1);

	//set upper rotor 1
	for(int i = 0; start1 != rotor1[upper][i];i++,offset++){
		/*char tempu = upperrotor[0];
		char templ = lowerrotor1[0];
		for(int j = 0; j < 25; j++){
			upperrotor[j] = upperrotor[j+1];
			lowerrotor1[j] = lowerrotor1[j+1];
		}
		upperrotor[25]= tempu;
		lowerrotor1[25]= templ;*/
		rotateRotor(rotor1,upper);
	}
	//set lower rotor 1
	for(int i = 0; i < offset;i++){
		/*char temp = lowerrotor1[0];
		for(int j = 0; j < 25; j++){
			lowerrotor1[j] = lowerrotor1[j+1];
		}
		lowerrotor1[25]= temp;*/
		rotateRotor(rotor1,lower);
	}

}

void setRotor2(char start2, int offset2){
	char upperrotor[]  = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
	char lowerrotor2[] = {'A','J','D','K','S','I','R','U','X','B','L','H','W','T','M','C','Q','G','Z','N','P','Y','F','V','O','E'};
    int offset = offset2;
    copyArray(rotor2,upper,upperrotor);
    copyArray(rotor2,upper,lowerrotor2);

	//set upper rotor 2
	for(int i = 0; start2 != rotor1[upper][1];i++){
		/*char tempu = upperrotor[0];
		char templ = lowerrotor2[0];
		for(int j = 0; j < 25; j++){
			upperrotor[j] = upperrotor[j+1];
			lowerrotor2[j] = lowerrotor2[j+1];
		}
		upperrotor[25]= tempu;
		lowerrotor2[25]= templ;*/
		rotateRotor(rotor2,upper);
	}
	//set lower rotor 1
	for(int i = 0; i < offset2;i++){
		/*char temp = lowerrotor2[0];
		for(int j = 0; j < 25; j++){
			lowerrotor2[j] = lowerrotor2[j+1];
		}
		lowerrotor2[25]= temp;*/
		rotateRotor(rotor2,upper);
	}

}

void setRotor3(char start3, int offset3){
	char upperrotor[]  = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
	char lowerrotor3[] = {'B','D','F','H','J','L','C','P','R','T','X','V','Z','N','Y','E','I','W','G','A','K','M','U','S','Q','O'};
    int offset = offset3;
    copyArray(rotor3,upper,upperrotor);
    copyArray(rotor3,upper,lowerrotor3);

	//set upper rotor 3
	for(int i = 0; start3 != rotor3[upper][i];i++){
		/*char tempu = upperrotor[0];
		char templ = lowerrotor3[0];
		for(int j = 0; j < 25; j++){
			upperrotor[j] = upperrotor[j+1];
			lowerrotor3[j] = lowerrotor3[j+1];
		}
		upperrotor[25]= tempu;
		lowerrotor3[25]= templ;*/
		rotateRotor(rotor3,upper);
	}
	//set lower rotor 3
	for(int i = 0; i < offset;i++){
		/*char temp = lowerrotor3[0];
		for(int j = 0; j < 25; j++){
			lowerrotor3[j] = lowerrotor3[j+1];
		}
		lowerrotor3[25]= temp;*/
		rotateRotor(rotor3,lower);
	}
}

void rotateRotor(char rotor[2][26],int layer){
	char temp = rotor[layer][0];
	for(int i = 0; i < 25;i++){
		rotor[layer][i] = rotor[layer][i+1];
	}
	rotor[layer][25] = temp;
}

void copyArray(char array[2][26],int layer, const char array2[26]){
	for(int i = 0; i < 26;i++){
		array[layer][i] = array2[i];
	}
}

char swap(char input, char* letterParen)
{
	char resultaat=input;
	for(int i=0;i<20;i++)
	{
		if((letterParen[i]==resultaat) && (i%2==0))
		{
			resultaat = letterParen[i+1];
			break;
		}
		else if((letterParen[i]==resultaat )&& (i%2!=0))
		{
			resultaat = letterParen[i-1];
			break;
		}
	}
	return resultaat;
}
