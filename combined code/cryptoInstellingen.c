#include "cryptoInstellingen.h"

char* getStartCharacters()
{
	static char startCharacters[3];
	return startCharacters;
}

void setStartCharacters(char* setStartCharacters)
{
	char* startCharactersArray=getStartCharacters();
	for(int i =0;i<3;i++)
	{
		startCharactersArray[i]=setStartCharacters[i];
	}
}

int* getOffSetRotors()
{
	static int offsetRotors[3];
	return offsetRotors;
}

void setOffsetInstelling(int* setArray)
{
	int* offsetArray= getOffSetRotors();
	offsetArray[0]=setArray[0];
	offsetArray[1]=setArray[1];
	offsetArray[2]=setArray[2];
}

char* getLetterpaar()
{
	static char letterParen[20];
	return letterParen;
}

void setLetterpaar(char* letterParenInput)
{
	char* letterArray = getLetterpaar();
	for(int i = 0;i<20;i++)
	{
		letterArray[i]=letterParenInput[i];
	}
}

int* getMaxChar()
{
	static int maxChar;
	return &maxChar;
}

void setMaxChar(int maxChar)
{
	int* referenceToMaxChar = getMaxChar();
	*referenceToMaxChar=maxChar;
}


