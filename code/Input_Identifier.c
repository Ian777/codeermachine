/*
 * Input_Identifier.c
 *
 *  Created on: 24 okt. 2018
 *      Author: jelle
 */


#include "Input_Identifier.h"

void getIdentity(int* array){
	int button_value = buttonsPressed();
	int dip_temp_value = dipPos();
	int dip_0_value = dip_temp_value & 0xff;
	int dip_1_value = (dip_temp_value>>8) & 0xff;
	int dip_2_value = (dip_temp_value>>16) & 0xff;
	array[1] = dip_0_value;
	array[2] = dip_1_value;
	array[3] = dip_2_value;
	int pressed_button = 0;
	for(int i=0; i<=26; i++){
		pressed_button = i;
		if((button_value & 0x1)==1){
			break;
		}
		else{
			button_value = button_value>>1;
		}
	}

	array[0] = pressed_button;
}
