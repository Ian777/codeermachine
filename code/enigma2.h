/*
 * enigma2.h
 *
 *  Created on: 16 nov. 2018
 *      Author: jelle
 */

#ifndef ENIGMA2_H_
#define ENIGMA2_H_

char codeer(char input);
void intitialize(char start1, int offset1, char start2, int offset2, char start3, int offset3);
void setRotor1(char start1, int offset1);
void setRotor2(char start2, int offset2);
void setRotor3(char start3, int offset3);
void rotateRotor(char rotor[2][26],int layer);
void copyArray(char array[2][26],int layer, const char array2[26]);

#endif /* ENIGMA2_H_ */
