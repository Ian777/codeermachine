/*
 * buttonDriver.c
 *
 *  Created on: 10 okt. 2018
 *      Author: jelle Raes
 */
#include "LPC17xx.h"
#include "buttonDriver.h"


int buttonsPressed(){
	selectpins();
	setMasks();
	setDirections();
	setPinmode();
	int value_port0 = LPC_GPIO0->FIOPIN;
	int semi_value1_port0 = value_port0 & (3); //2 bits long
	int semi_value2_port0 = (value_port0>>4) & (255); //8 bits long
	int semi_value3_port0 = (value_port0>>15) & (15); //4 bits long
	int semi_value4_port0 = (value_port0>>23) & (15); //4 bits long
	int corrected_value_port0 = (semi_value1_port0) | (semi_value2_port0<<2) | (semi_value3_port0<<10) | (semi_value4_port0<<14);
	int value_port1 = LPC_GPIO1->FIOPIN; //2 bits long
	int value_port2 = LPC_GPIO2->FIOPIN; //6 bits long
	int value = (corrected_value_port0) | (value_port1>>12) | (value_port2<<20);
	return value;
}

void selectpins(){
	LPC_PINCON->PINSEL0 &= ~(3<<18);
	LPC_PINCON->PINSEL0 &= ~(3<<16);
	LPC_PINCON->PINSEL0 &= ~(3<<14);
	LPC_PINCON->PINSEL0 &= ~(3<<12);
	LPC_PINCON->PINSEL0 &= ~(3<<0);
	LPC_PINCON->PINSEL0 &= ~(3<<2);
	LPC_PINCON->PINSEL1 &= ~(3<<4);
	LPC_PINCON->PINSEL1 &= ~(3<<2);
	LPC_PINCON->PINSEL0 &= ~(3<<30);
	LPC_PINCON->PINSEL1 &= ~(3<<0);
	LPC_PINCON->PINSEL1 &= ~(3<<14);
	LPC_PINCON->PINSEL1 &= ~(3<<16);
	LPC_PINCON->PINSEL1 &= ~(3<<18);
	LPC_PINCON->PINSEL1 &= ~(3<<20);
	LPC_PINCON->PINSEL3 &= ~(3<<28);
	LPC_PINCON->PINSEL3 &= ~(3<<30);
	LPC_PINCON->PINSEL4 &= ~(3<<10);
	LPC_PINCON->PINSEL4 &= ~(3<<8);
	LPC_PINCON->PINSEL4 &= ~(3<<6);
	LPC_PINCON->PINSEL4 &= ~(3<<4);
	LPC_PINCON->PINSEL4 &= ~(3<<2);
	LPC_PINCON->PINSEL4 &= ~(3<<0);
	LPC_PINCON->PINSEL0 &= ~(3<<22);
	LPC_PINCON->PINSEL0 &= ~(3<<20);
	LPC_PINCON->PINSEL0 &= ~(3<<10);
	LPC_PINCON->PINSEL0 &= ~(3<<8);
}

void setDirections(){
	LPC_GPIO0->FIODIR |= (0<<9);
	LPC_GPIO0->FIODIR |= (0<<8);
	LPC_GPIO0->FIODIR |= (0<<7);
	LPC_GPIO0->FIODIR |= (0<<6);
	LPC_GPIO0->FIODIR |= (0<<0);
	LPC_GPIO0->FIODIR |= (0<<1);
	LPC_GPIO0->FIODIR |= (0<<18);
	LPC_GPIO0->FIODIR |= (0<<17);
	LPC_GPIO0->FIODIR |= (0<<15);
	LPC_GPIO0->FIODIR |= (0<<16);
	LPC_GPIO0->FIODIR |= (0<<23);
	LPC_GPIO0->FIODIR |= (0<<24);
	LPC_GPIO0->FIODIR |= (0<<25);
	LPC_GPIO0->FIODIR |= (0<<26);
	LPC_GPIO1->FIODIR |= (0<<30);
	LPC_GPIO1->FIODIR |= (0<<31);
	LPC_GPIO2->FIODIR |= (0<<5);
	LPC_GPIO2->FIODIR |= (0<<4);
	LPC_GPIO2->FIODIR |= (0<<3);
	LPC_GPIO2->FIODIR |= (0<<2);
	LPC_GPIO2->FIODIR |= (0<<1);
	LPC_GPIO2->FIODIR |= (0<<0);
	LPC_GPIO0->FIODIR |= (0<<11);
	LPC_GPIO0->FIODIR |= (0<<10);
	LPC_GPIO0->FIODIR |= (0<<5);
	LPC_GPIO0->FIODIR |= (0<<4);
}

void setMasks(){
	LPC_GPIO0->FIOMASK = ~(0x07878FF3);
	LPC_GPIO1->FIOMASK = ~(0xC0000000);
	LPC_GPIO2->FIOMASK = ~(0x0000003F);
}

void setPinmode(){
	LPC_PINCON->PINMODE0 = 0xC0FFFFFF;
	LPC_PINCON->PINMODE1 = 0x003FFFFF;
	//LPC_PINCON->PINMODE2 = 0xF03F030F;
	LPC_PINCON->PINMODE3 = 0xFFFFFFFF;
	LPC_PINCON->PINMODE4 = 0x0FFFFFFF;
}
