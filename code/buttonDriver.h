/*
 * buttonDriver.h
 *
 *  Created on: 10 okt. 2018
 *      Author: jelle Raes
 */

#ifndef BUTTONDRIVER_H_
#define BUTTONDRIVER_H_
	int buttonsPressed();
	void selectpins();
	void setDirections();
	void setMasks();
	void setPinmode();
#endif /* BUTTONDRIVER_H_ */

/*
 * Next list shows the switch with the corresponding bit in the variable "value" in buttonsDriver.h
value is a integer that is 32bits long. the positions go from 0 to 31.

SW301 -> bit position 7
SW304 -> bit position 6
SW306 -> bit position 5
SW308 -> bit position 4
SW310 -> bit position 0
SW312 -> bit position 1
SW314 -> bit position 13
SW316 -> bit position 12
SW318 -> bit position 10
SW320 -> bit position 11
SW322 -> bit position 14
SW323 -> bit position 15
SW324 -> bit position 16
SW325 -> bit position 17
SW326 -> bit position 18
SW328 -> bit position 19
SW302 -> bit position 25
SW305 -> bit position 24
SW307 -> bit position 23
SW309 -> bit position 22
SW311 -> bit position 21
SW313 -> bit position 20
SW315 -> bit position 9
SW317 -> bit position 8
SW319 -> bit position 3
SW321 -> bit position 2

the list below shows the bit position with the corresponding switch.

bit position 0  -> SW310
bit position 1  -> SW312
bit position 2  -> SW321
bit position 3  -> SW319
bit position 4  -> SW308
bit position 5  -> SW306
bit position 6  -> SW304
bit position 7  -> SW301
bit position 8  -> SW317
bit position 9  -> SW315
bit position 10 -> SW318
bit position 11 -> SW320
bit position 12 -> SW316
bit position 13 -> SW314
bit position 14 -> SW322
bit position 15 -> SW323
bit position 16 -> SW324
bit position 17 -> SW325
bit position 18 -> SW326
bit position 19 -> SW328
bit position 20 -> SW313
bit position 21 -> SW311
bit position 22 -> SW309
bit position 23 -> SW307
bit position 24 -> SW305
bit position 25 -> SW302
 */
