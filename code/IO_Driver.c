/*
 * IO_Driver.c
 *
 *  Created on: 17 okt. 2018
 *      Author: noot
 */


#include "LPC17xx.h"
#include "IO_DRIVER.h"

void selectPinsIODip()
{
	LPC_PINCON->PINSEL0 &= ~(0b11111111000000001111);
	LPC_PINCON->PINSEL1 &= ~(0b1111<<2);
	LPC_PINCON->PINSEL0 &= ~(0b1111<<20);
	LPC_PINCON->PINSEL4 &= ~(0b111111111111);
	LPC_PINCON->PINSEL0 &= ~(0b11<<30);
	LPC_PINCON->PINSEL1 &= ~(0b1111111100000000000011);
	LPC_PINCON->PINSEL3 &= ~(0b1111<<28);
}

void IOPinsDirection()
{
	LPC_GPIO0->FIODIR &= ~(0xC7818783);
	LPC_GPIO1->FIODIR &= ~(0xC0000000);
	LPC_GPIO2->FIODIR &= ~(0x3F);
}

void IOPinMask()
{
	LPC_GPIO0->FIOMASK &= ~(0xC7818783);
	LPC_GPIO1->FIOMASK &= ~(0xC0000000);
	LPC_GPIO2->FIOMASK &= ~(0x3F);
}

int dipPos()
{
	int dipPosities=0;
	selectPinsIODip();
	IOPinsDirection();
	IOPinMask();
	dipPosities |= ((LPC_GPIO0->FIOPIN)&=(0b1<<17));//sw0,DIP0
	dipPosities |= ((LPC_GPIO0->FIOPIN)&=(0b1<<18))<<1;//sw0,DIP1
	dipPosities |= ((LPC_GPIO0->FIOPIN)&=(0b1<<1))<<2;//sw0,DIP2
	dipPosities |= ((LPC_GPIO0->FIOPIN)&=(0b1))<<3;//sw0,DIP3
	dipPosities |= ((LPC_GPIO0->FIOPIN)&=(0b1<<6))<<4;//sw0,DIP4
	dipPosities |= ((LPC_GPIO0->FIOPIN)&=(0b1<<7))<<5;//sw0,DIP5
	dipPosities |= ((LPC_GPIO0->FIOPIN)&=(0b1<<8))<<6;//sw0,DIP6
	dipPosities |= ((LPC_GPIO0->FIOPIN)&=(0b1<<9))<<7;//sw0,DIP7
	dipPosities |= ((LPC_GPIO0->FIOPIN)&=(0b1<<10))<<8;//sw1,DIP0
	dipPosities |= ((LPC_GPIO0->FIOPIN)&=(0b1<<11))<<9;//sw1,DIP1
	dipPosities |= ((LPC_GPIO2->FIOPIN)&=(0b1))<<10;//sw1,DIP2
	dipPosities |= ((LPC_GPIO2->FIOPIN)&=(0b1<<1))<<11;//sw1,DIP3
	dipPosities |= ((LPC_GPIO2->FIOPIN)&=(0b1<<2))<<12;//sw1,DIP4
	dipPosities |= ((LPC_GPIO2->FIOPIN)&=(0b1<<3))<<13;//sw1,DIP5
	dipPosities |= ((LPC_GPIO2->FIOPIN)&=(0b1<<4))<<14;//sw1,DIP6
	dipPosities |= ((LPC_GPIO2->FIOPIN)&=(0b1<<5))<<15;//sw1,DIP7
	dipPosities |= ((LPC_GPIO1->FIOPIN)&=(0b1<<31))<<16;//sw2,DIP0
	dipPosities |= ((LPC_GPIO1->FIOPIN)&=(0b1<<30))<<17;//sw2,DIP1
	dipPosities |= ((LPC_GPIO0->FIOPIN)&=(0b1<<26))<<18;//sw2,DIP2
	dipPosities |= ((LPC_GPIO0->FIOPIN)&=(0b1<<25))<<19;//sw2,DIP3
	dipPosities |= ((LPC_GPIO0->FIOPIN)&=(0b1<<24))<<20;//sw2,DIP4
	dipPosities |= ((LPC_GPIO0->FIOPIN)&=(0b1<<23))<<21;//sw2,DIP5
	dipPosities |= ((LPC_GPIO0->FIOPIN)&=(0b1<<16))<<22;//sw2,DIP6
	dipPosities |= ((LPC_GPIO0->FIOPIN)&=(0b1<<15))<<23;//sw2,DIP7
	return dipPosities;
}
