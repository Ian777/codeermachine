/*
 * enigma.c
 *
 *  Created on: 7 nov. 2018
 *      Author: jelle
 */

/*
#include "enigma.h"
char* rotor1[2];={
	   	   	   	{"ABCDEFGHIJKLMNOPQRSTUVWXYZ"},
				{"EKMFLGDQVZNTOWYHXUSPAIBRCJ"}
				}
char* rotor2[2];={
				{"ABCDEFGHIJKLMNOPQRSTUVWXYZ"},
				{"AJDKSIRUXBLHWTMCQGZNPYFVOE"}
				}
char* rotor3[2];={
				{"ABCDEFGHIJKLMNOPQRSTUVWXYZ"},
				{"BDFHJLCPRTXVZNYEIWGAKMUSQO"}
				}
char* reflec[2]={"ABCDEFGHIJKLMNOPQRSTUVWXYZ",
				 "YRUHQSLDPXNGOKMIEBFZCWVJAT"};

char codeer(char input){
	printf("%c",input);printf("in codeer methode\n");
	char temp_char= '0';
	for(int i = 0;i<26;i++){
		if(input == (rotor1[0])[i]){
			temp_char = (rotor1[1])[i];
			break;
		}
	}
	for(int i = 0;i<26;i++){
		if(temp_char == (rotor2[0])[i]){
			temp_char = (rotor2[1])[i];
			break;
		}
	}
	for(int i = 0;i<26;i++){
		if(temp_char == (rotor3[0])[i]){
			temp_char = (rotor3[1])[i];
			break;
		}
	}

	for(int i = 0;i<26;i++){
		if(temp_char == (reflec[0])[i]){
			temp_char = (reflec[1])[i];
			break;
		}
	}

	for(int i = 0;i<26;i++){
		if(temp_char == (rotor3[1])[i]){
			temp_char = (rotor3[0])[i];
			break;
		}
	}
	for(int i = 0;i<26;i++){
		if(temp_char == (rotor2[1])[i]){
			temp_char = (rotor2[0])[i];
			break;
		}
	}
	for(int i = 0;i<26;i++){
		if(temp_char == (rotor1[1])[i]){
			temp_char = (rotor1[0])[i];
			break;
		}
	}
	return temp_char;
}

void intitialize(char start1, int offset1, char start2, int offset2, char start3, int offset3){
	setRotor1(start1, offset1);
	setRotor2(start2, offset2);
	setRotor3(start3, offset3);
}

void setRotor1(char start1, int offset1){
	char upperrotor[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	char lowerrotor1[]= "EKMFLGDQVZNTOWYHXUSPAIBRCJ";

	//set upper rotor 1
	for(int i = 0; start1 != upperrotor[i];i++){
		char tempu = upperrotor[0];
		char templ = lowerrotor1[0];
		for(int j = 0; j < 25; j++){
			upperrotor[j] = upperrotor[j+1];
			lowerrotor1[j] = lowerrotor1[j+1];
		}
		upperrotor[25]= tempu;
		lowerrotor1[25]= templ;
	}
	rotor1[0] = upperrotor;
	//set lower rotor 1
	for(int i = 0; i < offset1;i++){
		char temp = lowerrotor1[0];
		for(int j = 0; j < 25; j++){
			lowerrotor1[j] = lowerrotor1[j+1];
		}
		lowerrotor1[25]= temp;
	}
	rotor1[1]= (char*) lowerrotor1;

}

void setRotor2(char start2, int offset2){
	char upperrotor[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	char lowerrotor2[] = "AJDKSIRUXBLHWTMCQGZNPYFVOE";

	//set upper rotor 2
	for(int i = 0; start2 != upperrotor[i];i++){
		char tempu = upperrotor[0];
		char templ = lowerrotor2[0];
		for(int j = 0; j < 25; j++){
			upperrotor[j] = upperrotor[j+1];
			lowerrotor2[j] = lowerrotor2[j+1];
		}
		upperrotor[25]= tempu;
		lowerrotor2[25]= templ;
	}
	rotor2[0] = upperrotor;
	//set lower rotor 1
	for(int i = 0; i < offset2;i++){
		char temp = lowerrotor2[0];
		for(int j = 0; j < 25; j++){
			lowerrotor2[j] = lowerrotor2[j+1];
		}
		lowerrotor2[25]= temp;
	}
	rotor2[1]= lowerrotor2;

}

void setRotor3(char start3, int offset3){
	char upperrotor[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	char lowerrotor3[] = "BDFHJLCPRTXVZNYEIWGAKMUSQO";

	//set upper rotor 3
	for(int i = 0; start3 != upperrotor[i];i++){
		char tempu = upperrotor[0];
		char templ = lowerrotor3[0];
		for(int j = 0; j < 25; j++){
			upperrotor[j] = upperrotor[j+1];
			lowerrotor3[j] = lowerrotor3[j+1];
		}
		upperrotor[25]= tempu;
		lowerrotor3[25]= templ;
	}
	rotor3[0] = upperrotor;
	//set lower rotor 3
	for(int i = 0; i < offset3;i++){
		char temp = lowerrotor3[0];
		for(int j = 0; j < 25; j++){
			lowerrotor3[j] = lowerrotor3[j+1];
		}
		lowerrotor3[25]= temp;
	}
	rotor3[1]= lowerrotor3;

}
*/
