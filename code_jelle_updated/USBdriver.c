/*
 * USBdriver.c
 *
 *  Created on: 17 okt. 2018
 *      Author: jelle
 */


#include "LPC17xx.h"
#include "USBdriver.h"
/*
 * D+ LPC_PORT 0.29 :: pinsel1 :: 27:26 :: GPIO0
 * D- LPC_PORT 0.30 :: pinsel1 :: 29:28 :: GPIO0
 */

void getPins(){
	//LPC_SC->PCONP |= (1<<31);
	LPC_PINCON->PINSEL1 &= ~(3<<26);
	LPC_PINCON->PINSEL1 &= ~(3<<28);

	LPC_GPIO0->FIOMASK = ~(0x60000000);

	LPC_GPIO0->FIODIR |= (0<<30);
	LPC_GPIO0->FIODIR |= (0<<29);

}
