/*
 * IO_Driver.c
 *
 *  Created on: 17 okt. 2018
 *      Author: noot
 */


#include "LPC17xx.h"
#include "IO_DRIVER.h"

void selectPinsIODip()
{
	/*LPC_PINCON->PINSEL0 &= ~(0b11111111000000001111);
	LPC_PINCON->PINSEL1 &= ~(0b1111<<2);
	LPC_PINCON->PINSEL0 &= ~(0b1111<<20);
	LPC_PINCON->PINSEL4 &= ~(0b111111111111);
	LPC_PINCON->PINSEL0 &= ~(0b11<<30);
	LPC_PINCON->PINSEL1 &= ~(0b1111111100000000000011);
	LPC_PINCON->PINSEL3 &= ~(0b1111<<28);*/

	LPC_PINCON->PINSEL0 &= ~(3<<18);
	LPC_PINCON->PINSEL0 &= ~(3<<16);
	LPC_PINCON->PINSEL0 &= ~(3<<14);
	LPC_PINCON->PINSEL0 &= ~(3<<12);
	LPC_PINCON->PINSEL0 &= ~(3<<0);
	LPC_PINCON->PINSEL0 &= ~(3<<2);
	LPC_PINCON->PINSEL1 &= ~(3<<4);
	LPC_PINCON->PINSEL1 &= ~(3<<2);
	LPC_PINCON->PINSEL0 &= ~(3<<30);
	LPC_PINCON->PINSEL1 &= ~(3<<0);
	LPC_PINCON->PINSEL1 &= ~(3<<14);
	LPC_PINCON->PINSEL1 &= ~(3<<16);
	LPC_PINCON->PINSEL1 &= ~(3<<18);
	LPC_PINCON->PINSEL1 &= ~(3<<20);
	LPC_PINCON->PINSEL3 &= ~(3<<28);
	LPC_PINCON->PINSEL3 &= ~(3<<30);
	LPC_PINCON->PINSEL4 &= ~(3<<10);
	LPC_PINCON->PINSEL4 &= ~(3<<8);
	LPC_PINCON->PINSEL4 &= ~(3<<6);
	LPC_PINCON->PINSEL4 &= ~(3<<4);
	LPC_PINCON->PINSEL4 &= ~(3<<2);
	LPC_PINCON->PINSEL4 &= ~(3<<0);
	LPC_PINCON->PINSEL0 &= ~(3<<22);
	LPC_PINCON->PINSEL0 &= ~(3<<20);
	LPC_PINCON->PINSEL0 &= ~(3<<10);
	LPC_PINCON->PINSEL0 &= ~(3<<8);
}

void IOPinsDirection()
{
	/*LPC_GPIO0->FIODIR &= ~(0xC7818783);
	LPC_GPIO1->FIODIR &= ~(0xC0000000);
	LPC_GPIO2->FIODIR &= ~(0x3F);*/

	LPC_GPIO0->FIODIR |= (0<<9);
	LPC_GPIO0->FIODIR |= (0<<8);
	LPC_GPIO0->FIODIR |= (0<<7);
	LPC_GPIO0->FIODIR |= (0<<6);
	LPC_GPIO0->FIODIR |= (0<<0);
	LPC_GPIO0->FIODIR |= (0<<1);
	LPC_GPIO0->FIODIR |= (0<<18);
	LPC_GPIO0->FIODIR |= (0<<17);
	LPC_GPIO0->FIODIR |= (0<<15);
	LPC_GPIO0->FIODIR |= (0<<16);
	LPC_GPIO0->FIODIR |= (0<<23);
	LPC_GPIO0->FIODIR |= (0<<24);
	LPC_GPIO0->FIODIR |= (0<<25);
	LPC_GPIO0->FIODIR |= (0<<26);
	LPC_GPIO1->FIODIR |= (0<<30);
	LPC_GPIO1->FIODIR |= (0<<31);
	LPC_GPIO2->FIODIR |= (0<<5);
	LPC_GPIO2->FIODIR |= (0<<4);
	LPC_GPIO2->FIODIR |= (0<<3);
	LPC_GPIO2->FIODIR |= (0<<2);
	LPC_GPIO2->FIODIR |= (0<<1);
	LPC_GPIO2->FIODIR |= (0<<0);
	LPC_GPIO0->FIODIR |= (0<<11);
	LPC_GPIO0->FIODIR |= (0<<10);
	LPC_GPIO0->FIODIR |= (0<<5);
	LPC_GPIO0->FIODIR |= (0<<4);
}

void IOPinMask()
{
	LPC_GPIO0->FIOMASK &= ~(0xC7818783);
	LPC_GPIO1->FIOMASK &= ~(0xC0000000);
	LPC_GPIO2->FIOMASK &= ~(0x3F);
}

int dipPos()
{
	int dipPosities=0;
	selectPinsIODip();
	IOPinsDirection();
	IOPinMask();
	setPinmodeIO();
	int poort[3];
	poort[0]=LPC_GPIO0->FIOPIN;
	poort[1]=LPC_GPIO1->FIOPIN;
	poort[2]=LPC_GPIO2->FIOPIN;
	printf("dips poort1 %d poort2 %d poort3 %d\n",poort[0],poort[1],poort[2]);
	dipPosities |= (((poort[0])&(0b1<<17))>>17);//sw0,DIP0
	dipPosities |= (((poort[0])&(0b1<<18))>>17);//sw0,DIP1
	dipPosities |= (((poort[0])&(0b1<<1))<<1);//sw0,DIP2
	dipPosities |= ((poort[0])&(0b1))<<3;//sw0,DIP3
	dipPosities |= ((poort[0])&(0b1<<6))>>2;//sw0,DIP4
	dipPosities |= ((poort[0])&(0b1<<7))>>2;//sw0,DIP5
	dipPosities |= ((poort[0])&(0b1<<8))>>2;//sw0,DIP6
	dipPosities |= ((poort[0])&(0b1<<9))>>2;//sw0,DIP7

	dipPosities |= ((poort[0])&(0b1<<10))>>2;//sw1,DIP0
	dipPosities |= ((poort[0])&(0b1<<11))>>2;//sw1,DIP1
	dipPosities |= ((poort[2])&(0b1))<<10;//sw1,DIP2
	dipPosities |= ((poort[2])&(0b1<<1))<<10;//sw1,DIP3
	dipPosities |= ((poort[2])&(0b1<<2))<<10;//sw1,DIP4
	dipPosities |= ((poort[2])&(0b1<<3))<<10;//sw1,DIP5
	dipPosities |= ((poort[2])&(0b1<<4))<<10;//sw1,DIP6
	dipPosities |= ((poort[2])&(0b1<<5))<<10;//sw1,DIP7

	dipPosities |= ((poort[1])&(0b1<<31))>>15;//sw2,DIP0
	dipPosities |= ((poort[1])&(0b1<<30))>>13;//sw2,DIP1
	dipPosities |= ((poort[0])&(0b1<<26))>>8;//sw2,DIP2
	dipPosities |= ((poort[0])&(0b1<<25))>>6;//sw2,DIP3
	dipPosities |= ((poort[0])&(0b1<<24))>>4;//sw2,DIP4
	dipPosities |= ((poort[0])&(0b1<<23))>>2;//sw2,DIP5
	dipPosities |= ((poort[0])&(0b1<<16))<<6;//sw2,DIP6
	dipPosities |= ((poort[0])&(0b1<<15))<<8;//sw2,DIP7
	return ~(dipPosities);
}

void setPinmodeIO(){
	/*
	//pull down resistor
	LPC_PINCON->PINMODE0 = 0xC0FFFFFF;
	LPC_PINCON->PINMODE1 = 0x003FFFFF;
	//LPC_PINCON->PINMODE2 = 0xF03F030F;
	LPC_PINCON->PINMODE3 = 0xFFFFFFFF;
	LPC_PINCON->PINMODE4 = 0x0FFFFFFF;*/

	//pull up
	LPC_PINCON->PINMODE0 = 0x0;
	LPC_PINCON->PINMODE1 = 0x00;
	//LPC_PINCON->PINMODE2 = 0x0;
	LPC_PINCON->PINMODE3 = 0x0;
	LPC_PINCON->PINMODE4 = 0x0;
}

