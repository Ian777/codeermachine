/*
 * Input_Identifier.h
 *
 *  Created on: 24 okt. 2018
 *      Author: jelle
 */

#ifndef INPUT_IDENTIFIER_H_
#define INPUT_IDENTIFIER_H_

#include "buttonDriver.h"
#include "IO_PIN_Driver.h"

void getIdentity(int* array);

#endif /* INPUT_IDENTIFIER_H_ */
